<?php 

session_start();

//List of product ids that the user will order
if(!array_key_exists("cart", $_SESSION)) {
	$_SESSION["cart"] = [];
}

if(!array_key_exists($_GET["id"], $_SESSION["cart"])) {
	$_SESSION["cart"][$_GET["id"]] = 0;
}

$_SESSION["cart"][$_GET["id"]] += 1;

echo count($_SESSION["cart"]);
?>