<?php 
session_start();

if(!isset($_GET["id"])) {
	echo "No ID was received";
	return;
}

$id = $_GET["id"];

//Check if id is existing
if(!array_key_exists($id, $_SESSION["cart"])) {
	echo "ID does not exist in cart";
	return;
}

//unset() destroys the specified variables.
unset($_SESSION["cart"][$id]);

header("Location: {$_SERVER["HTTP_REFERER"]}");
?>