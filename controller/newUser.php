<?php 

	session_start();

	$email = $_POST["email"];
	$firstname = $_POST["firstName"];
	$lastname = $_POST["lastName"];
	$password = $_POST["password"];
	$confirm_password = $_POST["confirm_password"];

	if(	!$email ||
		!$firstname ||
		!$lastname ||
		!$password ||
		!$confirm_password
	) {
		$_SESSION["error"] = "Please fill up all required fields";
		header("Location: {$_SERVER["HTTP_REFERER"]}");
		return;
	}


//validate if password is the same with confirm_password

if($password != $confirm_password) {
	$_SESSION["error"] = "Password and confirm password doest match";
	header("Location: {$_SERVER["HTTP_REFERER"]}");
	return;
}


require_once "connection.php";

//Validate if email is already taken
$query = "SELECT email FROM users WHERE email = '$email';";

echo $query;
//Trying to access array offset("email") on value of type null
$result = mysqli_fetch_assoc(mysqli_query($link, $query));


if($result) {
	$_SESSION["error"] = "Email is already taken";
	header("Location: {$_SERVER["HTTP_REFERER"]}");
	return;
}

$hashedPassword = password_hash($password, PASSWORD_DEFAULT);

$query = "
	INSERT INTO users(
		email,
		password,
		firstname,
		lastname
	) VALUES (
		'$email',
		'$hashedPassword',
		'$firstname',
		'$lastname'
	);
";

$result = mysqli_query($link, $query);

if(!result) {
	echo "Something went wrong";
	return;
}

$query = "SELECT * FROM users email = '$email'";
$user = mysqli_fetch_assoc(mysqli_query($link, $query));


// We store the details of the newly registered user in session so that we can treat the user as logged in after registration.
$_SESSION["user"] = $user;

header("Location: ../views/home.php");

?>