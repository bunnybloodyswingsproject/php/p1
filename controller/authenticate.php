<?php
session_start();

//Receive Data
$email = $_POST["email"];
$password = $_POST["password"];

//Validate
if(!$email || !$password) {
	$_SESSION["error"] = "Please fill-out all required fields";
	header("Location: ../views/login.php");
	return;
}

//Sanitize

//Process
require_once "connection.php";

$query = "SELECT * FROM users WHERE email = '$email'";
$result = mysqli_fetch_assoc(mysqli_query($link, $query));

if(!$result) {
	$_SESSION["error"] = "Email or password is incorrect";
	header("Location: ../views/login.php");
	return;
}

// how do we check if the password is correct???
if(!password_verify($password, $result["password"])) {
	$_SESSION["error"] = "Password is incorrect";
	header("Location: ../views/login.php");
	return;
}

// Store User Info to Session
$_SESSION["user"] = $result;

header("Location: ../views/home.php");

?>