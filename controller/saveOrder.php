<?php
	session_start();

	$email = $_POST["email"];
	$firstName = $_POST["firstName"];
	$lastName = $_POST["lastName"];
	$address = $_POST["address"];
	$postalCode = $_POST["postalCode"];
	$city = $_POST["city"];
	$region = $_POST["region"];
	$phone = $_POST["phone"];

	if(	!$email ||
		!$firstName ||
		!$lastName ||
		!$address ||
		!$postalCode ||
		!$city ||
		!$region ||
		!$phone
	) {
		$_SESSION["error"] = "Please fill up all required fields";
		header("Location: {$_SERVER["HTTP_REFERER"]}");
		return;
	}

	if(!isset($_POST["notice"])) {
		echo "Please check the the terms and agreements";
		return;
	}

	if(!is_numeric($postalCode)) {
		$_SESSION["error"] = "Invalid form: Postal Code Should be in number form";
		header("Location: {$_SERVER["HTTP_REFERER"]}");
		return;
	}

	if(!is_numeric($phone)) {
		$_SESSION["error"] = "Invalid form: Phone number Should be in number form";
		header("Location: {$_SERVER["HTTP_REFERER"]}");
		return;
	}

	require_once "connection.php";

	// Get the current date to be used to generate a transaction code
	$date = getdate();

	//Purchase
	$purchase_date = $date["year"] . "-" . $date["mon"] . "-" . $date["mday"] . " " . $date["hours"] . ":" . $date["minutes"] . ":" . $date["seconds"];

	print_r($purchase_date);

	$user_id = $_SESSION["user"]["id"];

	$query = "
		INSERT INTO orders(
			purchase_date,
			user_id,
			address,
			email,
			firstname,
			lastname,
			postalCode,
			city,
			region,
			phone_number
		) VALUES (
			'$purchase_date',
			'$user_id',
			'$address',
			'$email',
			'$firstName',
			'$lastName',
			'$postalCode',
			'$city',
			'$region',
			'$phone'
		)
	";

	if(mysqli_query($link, $query)) {
		//get last insert id
		$id = mysqli_insert_id($link);
		//create a transaction no.
		$transaction_no = $date["year"] . str_pad($date["mon"], 2, "0", STR_PAD_LEFT) . str_pad($date["mday"], 2 , "0", STR_PAD_LEFT) . $id;
		//Update order
		$query = "UPDATE orders SET transaction_no = '$transaction_no' WHERE id = $id";
		mysqli_query($link, $query);
	}

// Save all products stored in session cart to products_orders table
	// price
	// quantity
	// product_id
	// order id


// [
// 	1 => 5,
// 	51 => 1
// ]

$listOfIds = array_keys($_SESSION["cart"]); //[1, 51] => this will list all the existing Ids in the SESSION
$ids = implode(",", $listOfIds); // 1 , 51 => works likes a JSON_stringify

$query = "SELECT id, price FROM products WHERE id IN ($ids)";

$result = mysqli_query($link, $query);

// SELECT id, price FROM products WHERE id IN (1,51)

// $query = "
// 	INSERT INTO products_orders (
// 		price,
// 		quantity,
// 		product_id,
// 		order_id
// 	) VALUES ()
// "

$cart = $_SESSION["cart"];
$query = "INSERT INTO products_orders (
			price,
			quantity,
			product_id,
			order_id
) VALUES";

foreach($result as $item) {
	$query .= "(
			{$item["price"]},
			{$cart[$item["id"]]},
			{$item["id"]},
			$id
),";
}

$query = substr($query, 0, -1);
// INSERT INTO products_orders (price, quantity, product_id, order_id) VALUES (), (), ()
mysqli_query($link, $query);

$_SESSION["cart"] = [];

//$_SESSION["success"] = $transaction_no;
header("Location: ../views/transaciton_details.php?transaction_id={$transaction_no}");

?>

