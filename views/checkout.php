<?php 
	session_start();
	$path = "..";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Celigina</title>

	<!-- Metatags -->

	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">
	<meta charset="utf-8">
   	<meta name="description" content="online shopping website for fashion clothes, customized cakes, and jewelry">
	<link rel="shortcut icon" type="image/png" href="../assets/images/sayugi5_logo7.png">

   <!-- Fontawesome -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <script src="https://use.fontawesome.com/1a8df02521.js"></script>
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

   <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&family=Potta+One&display=swap" rel="stylesheet">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

   <!-- Custom CSS -->
   <link rel="stylesheet" type="text/css" href="<?= $path ?>/assets/css/style.css">

   <!-- LightBox CSS -->
   <link rel="stylesheet" type="text/css" href="<?php $path ?>/assets/css/lightbox.min.css">

   <!--Lightbox JS -->
   <script type="text/javascript" src="<?php $path ?>/assets/js/lightbox-plus-jquery.min.js"></script>


</head>
<body>

	<div class="wrapperCart">
		<section class="cart">
			<div class="cart_container">
				<div class="cart_title">
					<div class="cart_logo">
						<i class="fas fa-money-bill-wave"></i>
						<h4>Checkout</h4>
					</div>
					<div class="backToHomePage">
						<a href="<?php $path ?>/views/home.php">
							<i class="fas fa-times"></i>
						</a>
					</div>
				</div>

				<?php
				if(array_key_exists("error", $_SESSION)) {
				?>
				<div class="alert alert-danger dangerAlert">
					<button type="button" class="close" data-dismiss="alert">&times;</button>

					<?= $_SESSION["error"] ?>
				</div>

				<?php
				unset($_SESSION["error"]);

				}
				?>
				<?php
				require_once "../controller/connection.php";

				// GET all product details
				$products = [];
				foreach($_SESSION["cart"] as $id => $quantity) {
					$query = "SELECT * FROM products WHERE id = $id";
					$result = mysqli_fetch_assoc(mysqli_query($link, $query));
					array_push($products, $result);
				}

				$grandTotal = 0;
				$total = 0;
				if(count($products) == 0) {
					$shippingFee = 0;
					$_SESSION["error"] = "Please Shop with us first";
					header("Location: ../views/home.php");
				}

				if(count($_SESSION["cart"]) != 0 || count($_SESSION["cart"]) <= 4) {
					$shippingFee = 200;
				}

				if(count($_SESSION["cart"]) == 0 || count($_SESSION["cart"]) >= 5) {
					$shippingFee = 0;
				}

				foreach($products as $key => $product) {
					$key += 1;
					if($product["image_url"] != "https://via.placeholder.com/150") {
						$product["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $product["image_url"];
					}

				$subtotal = $product["price"] * $_SESSION["cart"][$product["id"]];
				$total += $subtotal;
				$grandTotal = $total + $shippingFee;

				?>

				<div class="cart_box">
					<div class="cart_list">
						<div class="cart_details">
							<div class="cart_info">
								<p class="cartIndex"><?= $key ?></p>
								<img src="<?= $product["image_url"] ?>">
								<div class="cartProductName">
									<p><?= $product["name"] ?></p>
									<p class="cartPrice"><?= intval($product["price"]) ?></p>
									<p class="subPrice" hidden><?= $subtotal ?></p>
								</div>
							</div>
						</div>
				
					</div>
				</div>

				<?php
				}
				?>

				<div class="totalCalculator">
					<div class="subtotal">
						<div class="total">
							<p class="totalSubs">Subtotal</p>
							<p class="totalNumbers" id="subPriceTotal">$<?= $total ?></p>
						</div>
						<div class="total">
							<p class="totalSubs">Shipping</p>
							<p class="totalNumbers" id="shippingFee">$<?= $shippingFee ?></p>
						</div>
					</div>
					<div class="grandTotal">
						<div class="total">
							<p class="totalSubs">Grand Total</p>
							<p class="totalNumbers" id="grandTotal">$ <?= $grandTotal ?></p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="checkForm sub_section">
			<div class="check_container">
				<div class="checkout_img">
					<img src="<?php $path ?>/assets/images/sayugi5_logo5.png">
				</div>
				<div class="checkout_form">
					<h4>SHIPPING ADDRESS</h4>
					<form action="../controller/saveOrder.php" method="POST">
						<input type="email" name="email" placeholder="EMAIL" class="form-control fullInput">
						<div class="form_halfInput">
							<input type="text" name="firstName" placeholder="First Name" class="form-control halfInput">
							<input type="text" name="lastName" placeholder="Last Name" class="form-control halfInput">
						</div>
						<input type="text" name="address" placeholder="Complete Address" class="form-control fullInput">
						<div class="form_halfInput">
							<input type="text" name="postalCode" placeholder="Postal Code" class="form-control halfInput">
							<input type="text" name="city" placeholder="City" class="form-control halfInput">
						</div>
						<input type="text" name="region" placeholder="Region" class="form-control fullInput">
						<input type="text" name="country" placeholder="Philippines" class="form-control fullInput" value="Philippines" disabled>
						<input type="text" name="phone" placeholder="Phone Number" class="form-control fullInput">
						<div class="terms_and_condition">
							<div class="terms_agree">
								<span class="required_text">*</span>
								<input type="checkbox" id="important_notice" name="notice" id="notice">
								<label class="important_terms">I agree on the <a href="#notice"><span>important notice</span></a> of the sayuji and <a href="<?php $path ?>/views/test.php" class="important_terms"><span>terms and condition</span></a></label>
							</div>
						</div>
						<div class="login_buttom">
							<button class="cartCheckout">Checkout</button>
						</div>
					</form>
				</div>
			</div>
		</section>

		<section class="important_notice sub_section">
			<div class="important_container">
				<h4 id="notice">IMPORTANT:</h4>
				<div class="authors_message">
					<p>HI Our Valued Customer,</p>
					<br>
					<p class="sentences">We, Sayuji, are doing our best to to give you the best service that we have in our shop. Due to the ongoing pandemic in our community, cancelation of the ordered products would be a hard process on our end.
					To avoid this process, kindly inform us on the same day of checkout of the product, so we can process this smoothly.</p>

					<br>

					<p class="sentences">Failure to do this would lead us to extent such as posting your personal information on all the social media platform such as facebook, instagram, and twitter etc.</p>

					<br>

					<p class="sentences">We hope we will not arrive on this kind of conclusion. We cared for your well being as much as our servce.</p>

					<br>

					<p>Thank you.</p>

					<br>

					<p>Sayuji</p>

				</div>
			</div>
		</section>

		<!-- ========= Authors Guarantee section ========= -->
		<section class="Guanratee guanratee_box">
			<div class="seller_guarantee bd-grid">
				<ul class="sellersMessage">
					<li class="sellersPromise">
						<i class="fas fa-gem"></i>
						<p>We are doing our best to deliver the <span>best quality</span> of item to your home</p>
					</li>
					<li class="sellersPromise">
						<i class="fas fa-truck"></i>
						<p><span>3-7 days ship nationwide</span> Cash on Delivery.</p>
					</li>
					<li class="sellersPromise">
						<i class="fas fa-shield-alt"></i>
						<p>
							<span>7-Day Return Guarantee</span>
							except from items like cakes and bread.
							Return damaged products without additional fees.
						</p>
					</li>
				</ul>
			</div>
		</section>
	<div class="pushCart"></div>
  </div>
  <footer class="footer section text-center py-4 text-dark">
  	<small>&copy; 2021 SAYUJI</small>
  </footer>

   <!-- CDN -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   
   <!-- Custom JS -->
   <script src="<?= $path ?>/assets/js/script.js"></script>
</body>
</html>
