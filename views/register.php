<?php $path = "..";
session_start();
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Celigina</title>

	<!-- Metatags -->

	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">
	<meta charset="utf-8">
   	<meta name="description" content="online shopping website for fashion clothes, customized cakes, and jewelry">
	<link rel="shortcut icon" type="image/png" href="../assets/images/sayugi5_logo7.png">

   <!-- Fontawesome -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <script src="https://use.fontawesome.com/1a8df02521.js"></script>
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

   <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&family=Potta+One&display=swap" rel="stylesheet">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

   <!-- Custom CSS -->
   <link rel="stylesheet" type="text/css" href="<?= $path ?>/assets/css/style.css">

   <!-- LightBox CSS -->
   <link rel="stylesheet" type="text/css" href="<?php $path ?>/assets/css/lightbox.min.css">

   <!--Lightbox JS -->
   <script type="text/javascript" src="<?php $path ?>/assets/js/lightbox-plus-jquery.min.js"></script>


</head>
<body>

	<div class="wrapper">
		<?php 
			if(array_key_exists("error", $_SESSION)) {
		?>

		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">&times;</button>

				<?= $_SESSION["error"] ?>
		</div>


		<?php
			unset($_SESSION["error"]);
			}
		?>
		<section class="login">
			<div class="login_container">
				<div class="class_form">
					<form action="<?php $path ?>/controller/newUser.php" method="POST">
						<div class="login_title">
							<a href="<?php ?>/views/home.php">
								<img src="<?php ?>/assets/images/sayugi5_logo7.png">
							</a>
							<h4 class="nav_logo">SAYUJI</h4>
						</div>
						<div class="divInputs">
							<div class="email_input">
								<i class="fas fa-user-tag"></i>
								<input type="text" name="email" class="form-control" placeholder="Email">
							</div>
							<div class="email_input">
								<i class="fas fa-user"></i>
								<input type="text" name="firstName" class="form-control" placeholder="First Name">
							</div>
							<div class="password_input">
								<i class="far fa-address-book"></i>
								<input type="text" name="lastName" class="form-control" placeholder="Last Name">
							</div>
							<div class="email_input">
								<i class="fas fa-key"></i>
								<input type="password" name="password" class="form-control" placeholder="Password">
							</div>
							<div class="password_input">
								<i class="fas fa-unlock-alt"></i>
								<input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password">
							</div>
						</div>
						<div class="login_buttom">
							<button class="loginBtn">Sign Up</button>
						</div>
					</form>
				</div>
			</div>
		</section>

	<div class="push"></div>
  </div>

  <footer class="footer section footer_login text-center py-4 text-dark">
		<div class="form_footer">
			<div class="fb_links">
				<i class="fab fa-facebook"></i>
				<a href="#"><small>Check our FB page</small></a>
			</div>
		</div>
		<div>
			<div class="footer_logo">
				<small>&copy; <span>SAYUJI</span></small>
			</div>
		</div>
  </footer>


   <!-- CDN -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   
   <!-- Custom JS -->
   <script src="<?= $path ?>/assets/js/script.js"></script>
</body>
</html>
