<?php
	session_start();
	$path = "..";

	function getContent() {

?>	

	<?php
	if(array_key_exists("error", $_SESSION)) {
	?>

	<div class="alert alert-danger successAlert">
		<button type="button" class="close" data-dismiss="alert">&times;</button>

		<p><?= $_SESSION["error"] ?></p>
	</div>

	<?php
	unset($_SESSION["error"]);
	}
	?>

	<?php
	if(array_key_exists("success", $_SESSION)) {
	?>

	<div class="alert alert-success successAlert">
		<button type="button" class="close" data-dismiss="alert">&times;</button>

		<p>Thank you shopping with us! Heres your transaction Code: <?= $_SESSION["success"] ?></p>
	</div>

	<?php
	unset($_SESSION["success"]);
	}
	?>
	<!-- ======== HOME PAGE ======== -->
	<section class="main-1">
		<div class="home_container">
			<div class="slider">
				<div class="home_slides">
					<input type="radio" name="radio-btn" id="radio1">
					<input type="radio" name="radio-btn" id="radio2">
					<input type="radio" name="radio-btn" id="radio3">
					<input type="radio" name="radio-btn" id="radio4">

					<div class="slide first">
						<img src="<?php $path ?>/assets/images/slide1.jpg">
					</div>

					<div class="slide">
						<img src="<?php $path ?>/assets/images/slide8.jpg">
					</div>

					<div class="slide">
						<img src="<?php $path ?>/assets/images/slide7.jpg">
					</div>

					<div class="slide">
						<img src="<?php $path ?>/assets/images/slide5.jpg">
					</div>

					<!-- auto navigation start -->
					<div class="navigation-auto">
						<div class="auto-btn1"></div>
						<div class="auto-btn2"></div>
						<div class="auto-btn3"></div>
						<div class="auto-btn4"></div>
					</div>
					<!-- auto navigation end -->

					<!-- manual navigation start -->
					<div class="navigation-manual">
						<label for="radio1" class="manual-btn"></label>
						<label for="radio2" class="manual-btn"></label>
						<label for="radio3" class="manual-btn"></label>
						<label for="radio4" class="manual-btn"></label>
					</div>
					<!-- manual navigation end -->
				</div>
			</div>
		</div>
	</section>

	<!-- ======== Greetings PAGE ======== -->
	<section class="greetings section">
		<div class="greetings_container bd-grid">
			<div class="greetings_box">
				<h1>Welcome to <span>SAYUJI</span></h1>
				<p>Sayugi is a small family business where you can shop for the whole family. You can also buy yourself a treat. 
				<br>
				<br>
				Choose and shop a gift for your family, friends and love ones.</p>
			</div>
		</div>
		<img src="<?php $path ?>/assets/images/greetings1_img.jpg" class="greetings_img fade-In">
	</section>

	<!-- ======== OFFER PAGE ======== -->
	<section class="offer section">
		<div class="offer_bg">
			<div class="offer-data">
				<h2 class="offer_title">Special Offer</h2>
				<p class="offer_description">
					Looking for special offers and discounts in our shop? <br>
					Click the button below for more details
				</p>

				<a href="#" class="shopButton">Click Here!</a>
			</div>
		</div>
	</section>

	<!-- ======== NEW PRODUCTS PAGE ======== -->

	<section class="new section" id="new">
		<h2 class="section-title">FEATURED PRODUCTS</h2>
		<a href="#" class="section-all">View All</a>

		<div class="new_container bd-grid">
			<div class="new_box sliding from-left">
				<img src="<?php $path ?>/assets/images/new_img.png" class="new_img">
				<div class="new_description">
					<span class="ownerLable">Sweet Counter</span>
					<p class="itemTitle">Spiderman Number 4 Cake(Personalized)</p>
					<p class="itemPrice">₱2,500</p>
				</div>
				<div class="new_link">
					<a href="#" class="shopBtn">VIEW PRODUCT</a>
				</div>
			</div>

			<div class="new_box sliding from-left">
				<img src="<?php $path ?>/assets/images/new2_img.png" class="new_img">
				<div class="new_description">
					<span class="ownerLable">Sweet Counter</span>
					<p class="itemTitle">Special Birthdat Cake(Personalized)</p>
					<p class="itemPrice">₱2,500</p>
				</div>
				<div class="new_link">
					<a href="#" class="shopBtn">VIEW PRODUCT</a>
				</div>
			</div>

			<div class="new_box sliding from-right">
				<img src="<?php $path ?>/assets/images/new3_img.png" class="new_img">
				<div class="new_description">
					<span class="ownerLable">Celigina Clothing</span>
					<p class="itemTitle">Black Suit</p>
					<p class="itemPrice">₱999</p>
				</div>
				<div class="new_link">
					<a href="#" class="shopBtn">VIEW PRODUCT</a>
				</div>
			</div>

			<div class="new_box sliding from-right">
				<img src="<?php $path ?>/assets/images/new4_img.png" class="new_img">
				<div class="new_description">
					<span class="ownerLable">Celigina Clothing</span>
					<p class="itemTitle">Personalized Merch T-Shirt</p>
					<p class="itemPrice">₱550(each)</p>
				</div>
				<div class="new_link">
					<a href="#" class="shopBtn">VIEW PRODUCT</a>
				</div>
			</div>

		</div>
	</section>

	<section class="sponsor section">
		<div class="brand_intro">
			<h2 class="section-title">SELLING BRANDS</h2>
			<p class="brand_description">We believe and we care in what we sell. See it to yourself.
			<br>
			Shop with us</p>
		</div>
		<p class="category">CLOTHING</p>
		<div class="sponsors_container bd-grid sliding from-left">
			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand1_img.png">
			</div>

			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand2_img.png">
			</div>

			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand3_img.png">
			</div>

			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand4_img.png">
			</div>
		</div>

		<p class="category">BAKERY</p>
		<div class="sponsors_container bd-grid sliding from-right">
			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand5_img.png">
			</div>

			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand6_img.png">
			</div>

			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand7_img.png">
			</div>

			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand8_img.png">
			</div>
		</div>

		<p class="category">JEWELRY</p>
		<div class="sponsors_container bd-grid sliding from-left">
			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand9_img.png">
			</div>

			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand10_img.png">
			</div>

			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand11_img.png">
			</div>

			<div class="sponsors_logo">
				<img src="<?php $path ?>/assets/images/brand12_img.png">
			</div>
		</div>
	</section>

<?php 
	}
?>
<?php require_once $path . "/layouts/templates.php" ?>