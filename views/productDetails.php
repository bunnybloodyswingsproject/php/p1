<?php 
	session_start();
	$path = "..";
	function getContent() {

	require_once "../controller/connection.php";

	$id = $_GET['product_id'];

	$query = "
		SELECT products.name as name, products.price as price, products.description as description,image_url, categories.name as category FROM products JOIN categories ON (products.category_id = categories.id) WHERE products.id = {$id}
	";

	$results = mysqli_query($link, $query);

?>
	<?php
	//if were going to use foreach, use this format <?php not <?= or else there will be a parse error
	foreach ($results as $id => $product) {
		if($product["image_url"] != "https://via.placeholder.com/150") {
			$product["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $product["image_url"];
		}
	?>
	<section class="product_details">
		<div class="productDetails_box">
			<div class="product_img">
				<a href="<?= $product["image_url"] ?>" data-lightbox="productDetails">
					<img src="<?= $product["image_url"] ?>" class="productPreview">
				</a>
			</div>
		</div>
		<div class="product_specification">
			<div class="product_title">
				<div class="product_price">
					<h4><span class="pesoSign">₱</span><?php echo intval($product["price"]) ?></h4>
					<div class="discount">
						<i class="fas fa-tags"></i>
						<del><span>₱</span>3,500</del>
					</div>
				</div>

				<div class="productTitle">
					<h4><?php echo $product["name"] ?></h4>
				</div>
			</div>
			<div class="shopTag">
				<small class="itemOwner">
					<i class="fas fa-store"></i>
				Sweet Tooth</small>
				<small class="region">
				<i class="fas fa-truck"></i>
				National Capial Region and Calabarzon</small>
			</div>
		</div>
		<div class="divSeperator"></div>
		<div class="descriptionModal">
			<button class="modalBtn" id="modalBtn">
				<div class="descriptionBox">
					<p>Description</p>
					<i class="fas fa-angle-right"></i>
				</div>
				<ul class="briefDescription">
					<li><?php echo $product["description"] ?></li>
				</ul>
			</button>

			<div id="specificationModal" class="specificationModal">
				<div class="specsDescription">
					<p>Description</p>
					<span class="close closeModal">&times;</span>
				</div>
				<div class="specsContent">
					<div class="content">
						<p class="contentTitle">Details</p>
						<ul class="modalContent">
							<li><?php echo $product["description"] ?></li>
						</ul>
					</div>
					<div class="content">
						<p class="contentTitle">Category</p>
						<ul class="modalContent">
							<li><?php echo $product["category"] ?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="divSeperator"></div>

		<div class="descriptionModal">
			<button class="modalBtn" id="modalBtn2">
				<div class="descriptionBox">
					<p>Delivery</p>
					<i class="fas fa-angle-right"></i>
				</div>
				<ul class="briefDescription">
					<li>The Parcel is shipped by seller</li>
				</ul>
			</button>

			<div id="specificationModal2" class="specificationModal">
				<div class="specsDescription">
					<p>Delivery</p>
					<span class="close2 closeModal">&times;</span>
				</div>
				<div class="specsContent deliveryContent">
					<div class="content">
						<p class="contentTitle">Delivery Fee</p>
						<ul class="deliveryFee">
							<li>
								<i class="fas fa-truck-loading"></i>Standard
							</li>
							<li>₱200.00</li>
						</ul>
					</div>
				</div>
				<div class="deliveryContent">
					<div class="content">
						<p class="contentTitle">Delivery Fee</p>
						<ul class="deliveryFee">
							<li>
								<i class="fas fa-percent"></i>Free Shipping Every 5 item purchase
							</li>
							<li class="freeText">Free</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="divSeperator"></div>
	</section>

	<?php
	}
	?>

	<!-- ========= Special request section ========= -->
	<section class="request">
		<div class="special_request">
			<div class="request_message">
				<h4 class="request_header">Special Instruction</h4>
				<textarea class="requestBox" placeholder="Enter your message here"></textarea>
			</div>

			<div class="quantityManager">
				<div class="cartBtn">
					<input type="" name="" value="<?= $_GET['product_id'] ?>" hidden>
					<button class="addToCartBtn">ADD TO CART</button>
				</div>
			</div>
		</div>
	</section>

	<div class="divSeperator"></div>

	<!-- ========= Authors Guarantee section ========= -->
	<section class="Guanratee sub_section">
		<div class="seller_guarantee bd-grid">
			<div class="sayujiLogo">
				<img src="<?php $path ?>/assets/images/sayugi5_logo5.png">
			</div>
			<ul class="sellersMessage">
				<li class="sellersPromise">
					<i class="fas fa-gem"></i>
					<p>We are doing our best to deliver the <span>best quality</span> of item to your home</p>
				</li>
				<li class="sellersPromise">
					<i class="fas fa-truck"></i>
					<p><span>3-7 days ship nationwide</span> Cash on Delivery.</p>
				</li>
				<li class="sellersPromise">
					<i class="fas fa-shield-alt"></i>
					<p>
						<span>7-Day Return Guarantee</span>
						except from items like cakes and bread.
						Return damaged products without additional fees.
					</p>
				</li>
			</ul>
		</div>
	</section>

	<!-- ========= NEWSLETTER Slider ========= -->
	<section class="newsletter section">
		<div class="newsletter_container bd-grid">
			<div class="newsletter_subscribe">
				<h2 class="sectionTitle">OUR NEWSLETTER</h2>
				<p class="newsletter_description">Promotion new products and sales. Directly to your</p>

				<form class="newsletter_form">
					<input type="text" name="" class="newsletter_input" placeholder="Enter your email">
					<a href="#" class="newsBtn">SUBSCRIBE</a>
				</form>
			</div>
		</div>
	</section>
<?php
	}

	require_once $path . "/layouts/templates.php"
?>
