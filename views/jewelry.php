<?php 
	
	session_start();
	$path = "..";
	$storyPath = ".";

	function getContent() {
	require_once "../controller/connection.php";

	$query = "
	SELECT 
			products.id as 'id',
			products.name as 'name',
			price,
			image_url
			FROM products JOIN categories ON (products.category_id = categories.id)
			WHERE products.category_id = 10
";

$results = mysqli_query($link, $query);

?>

	<!-- ========= Box Slider ========= -->
	<section class="box_slider">
		<div class="slide_container">
			<button class="btn left-btn hide">
				<i class="fas fa-angle-left"></i>
			</button>
			<div class="carousel-container">
				<ul class="carousel_section">
					<li class="slides activeSlide">
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/sayuji.png" class="story">
								<p>Our Name</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry1.jpg" class="has-story">
								<p>Marleyan Necklace</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry8.jpg" class="has-story">
								<p>Blue Spahirre</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry2.jpg" class="has-story">
								<p>The Royal Beads</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry3.jpg" class="has-story">
								<p>Clover Leaf</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry4.jpg" class="has-story">
								<p>Blueberry</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry8.jpg" class="has-story">
								<p>Blue Spahirre</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry7.jpg" class="has-story">
								<p>Infinity Necklace</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry1.jpg" class="has-story">
								<p>Marleyan Necklace</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry7.jpg" class="has-story">
								<p>Infinity Necklace</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry1.jpg" class="has-story">
								<p>Marleyan Necklace</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry8.jpg" class="has-story">
								<p>Blue Spahirre</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry15.png" class="has-story">
								<p>Pearly Sea</p>
							</a>
						</div>
					</li>

					<li class="slides">
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry5.jpg" class="story">
								<p>Green Jade</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry6.jpg" class="has-story">
								<p>Diamond Ring</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry7.jpg" class="has-story">
								<p>Infinity Necklace</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry8.jpg" class="has-story">
								<p>Blue Spahirre</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry9.jpg" class="has-story">
								<p>The Ring</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry1.jpg" class="has-story">
								<p>Marleyan Necklace</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry7.jpg" class="has-story">
								<p>Infinity Necklace</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry8.jpg" class="has-story">
								<p>Blue Spahirre</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry11.jpg" class="has-story">
								<p>Rainbow Brace</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry10.jpg" class="story">
								<p>Plain Silver</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry7.jpg" class="has-story">
								<p>Infinity Necklace</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry12.jpg" class="has-story">
								<p>Royal Beads</p>
							</a>
						</div>
					</li>

					<li class="slides">
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry10.jpg" class="story">
								<p>Plain Silver</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry11.jpg" class="has-story">
								<p>Rainbow Brace</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry12.jpg" class="has-story">
								<p>Royal Beads</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry7.jpg" class="has-story">
								<p>Infinity Necklace</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry14.jpg" class="has-story">
								<p>The Grandmaster</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry15.png" class="has-story">
								<p>Pearly Sea</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry8.jpg" class="has-story">
								<p>Blue Spahirre</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry12.jpg" class="has-story">
								<p>Royal Beads</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry7.jpg" class="has-story">
								<p>Infinity Necklace</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry11.jpg" class="has-story">
								<p>Rainbow Brace</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry12.jpg" class="has-story">
								<p>Royal Beads</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry8.jpg" class="has-story">
								<p>Blue Spahirre</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry7.jpg" class="has-story">
								<p>Infinity Necklace</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry12.jpg" class="has-story">
								<p>Royal Beads</p>
							</a>
						</div>
					</li>
				</ul>
			</div>
			<button class="btn right-btn">
				<i class="fas fa-angle-right nextButton"></i>
			</button>
		</div>	
	</section>

	<!-- ========= Item List For Clothing ========= -->
	<section class="clothing section">
		<div class="clothing_container bd-grid">
		<?php
			foreach ($results as $product) {
				if($product["image_url"] != "https://via.placeholder.com/150") {
					$product["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $product["image_url"];
				}
		?>

			<div class="box">
				<div class="slide-img">
					<img src="<?= $product["image_url"] ?>">
					<div class="overlay">
						<a href="<?php $path ?>/views/productDetails.php?product_id=<?= $product["id"] ?>" class="viewBtn">View Item</a>
					</div>
				</div>
				<div class="detail-box">
					<!-- ==== type ==== -->
					<div class="type">
						<a href="<?php $path ?>/views/productDetails.php?product_id=<?= $product["id"] ?>"><?= $product["name"] ?></a>
						<span>New Arrival</span>
					</div>
					<!-- ==== price ==== -->
					<a href="#" class="price">$<?= intval($product["price"]) ?></a>
				</div>
			</div>

		<?php
			}
		?>

		</div>
	</section>

	<!-- ========= NEWSLETTER Slider ========= -->
	<section class="newsletter section">
		<div class="newsletter_container bd-grid">
			<div class="newsletter_subscribe">
				<h2 class="sectionTitle">OUR NEWSLETTER</h2>
				<p class="newsletter_description">Promotion new products and sales. Directly to your</p>

				<form class="newsletter_form">
					<input type="text" name="" class="newsletter_input" placeholder="Enter your email">
					<a href="#" class="newsBtn">SUBSCRIBE</a>
				</form>
			</div>
		</div>
	</section>
<?php	
	}
?>

<?php require_once $path . "/layouts/templates.php" ?>