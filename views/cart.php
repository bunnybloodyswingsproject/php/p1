<?php 
	session_start();
	$path = "..";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Celigina</title>

	<!-- Metatags -->

	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">
	<meta charset="utf-8">
   	<meta name="description" content="online shopping website for fashion clothes, customized cakes, and jewelry">
	<link rel="shortcut icon" type="image/png" href="../assets/images/sayugi5_logo7.png">

   <!-- Fontawesome -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <script src="https://use.fontawesome.com/1a8df02521.js"></script>
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

   <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&family=Potta+One&display=swap" rel="stylesheet">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

   <!-- Custom CSS -->
   <link rel="stylesheet" type="text/css" href="<?= $path ?>/assets/css/style.css">

   <!-- LightBox CSS -->
   <link rel="stylesheet" type="text/css" href="<?php $path ?>/assets/css/lightbox.min.css">

   <!--Lightbox JS -->
   <script type="text/javascript" src="<?php $path ?>/assets/js/lightbox-plus-jquery.min.js"></script>


</head>
<body>

	<div class="wrapperCart">
		<section class="cart">
			<div class="cart_container">
				<div class="cart_title">
					<div class="cart_logo">
						<i class="fas fa-shopping-cart"></i>
						<h4>CART</h4>
					</div>
					<div class="backToHomePage">
						<a href="<?php $path ?>/views/home.php">
							<i class="fas fa-times"></i>
						</a>
					</div>
				</div>

				<?php
				require_once "../controller/connection.php";

				// GET all product details
				$products = [];
				foreach($_SESSION["cart"] as $id => $quantity) {
					$query = "SELECT * FROM products WHERE id = $id";
					$result = mysqli_fetch_assoc(mysqli_query($link, $query));

					array_push($products, $result);
				}

				?>

				<?php 

				$total = 0;
				if(count($products) == 0) {
					$shippingFee = 0;
				?>

				<div class="emptyCart">
					<p>No items available</p>
					<a href="<?php $path ?>/views/clothing.php" class="viewBtn">Go Shopping</a>
				</div>

				<?php
				}
				?>

				<?php
				$grandTotal = 0;

				if(count($_SESSION["cart"]) != 0 || count($_SESSION["cart"]) <= 4) {
					$shippingFee = 200;
				}

				if(count($_SESSION["cart"]) == 0 || count($_SESSION["cart"]) >= 5) {
					$shippingFee = 0;
				}

				foreach($products as $key => $product) {
					$key += 1;
					if($product["image_url"] != "https://via.placeholder.com/150") {
						$product["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $product["image_url"];
					}

					$subtotal = $product["price"] * $_SESSION["cart"][$product["id"]];
					$total += $subtotal;
					$grandTotal = $total + $shippingFee;
					
				?>

				<div class="cart_box">
					<div class="cart_list">
						<div class="cart_details">
							<div class="cart_info">
								<p class="cartIndex"><?= $key ?></p>
								<img src="<?= $product["image_url"] ?>">
								<div class="cartProductName">
									<p><?= $product["name"] ?></p>
									<p class="cartPrice"><?= intval($product["price"]) ?></p>
									<p class="subPrice" hidden><?= $subtotal ?></p>
								</div>
							</div>
						</div>
				
						<div class="plusController">
							<div class="quantityController">
								<i class="fas fa-plus btn-plus" data-id="<?= $product["id"] ?>"></i>
								<p><?= $_SESSION["cart"][$product["id"]] ?></p>
								<i class="fas fa-minus btn-minus" data-id="<?= $product["id"] ?>"></i>
							</div>
							<div>
								<a href="<?php $path ?>/controller/removeItemInCart.php?id=<?= $product["id"] ?>" class="cartCancelBtn">Cancel</a>
							</div>
						</div>
					</div>
				</div>

				<?php
				}
				?>
				<div class="totalCalculator">
					<div class="subtotal">
						<div class="total">
							<p class="totalSubs">Subtotal</p>
							<p class="totalNumbers" id="subPriceTotal">$<?= $total ?></p>
						</div>
						<div class="total">
							<p class="totalSubs">Shipping</p>
							<p class="totalNumbers" id="shippingFee">$<?= $shippingFee ?>
							</p>
						</div>
					</div>
					<div class="grandTotal">
						<div class="total">
							<p class="totalSubs">Grand Total</p>
							<p class="totalNumbers" id="grandTotal">$ <?= $grandTotal ?></p>
						</div>
					</div>
				</div>
			</div>
		</section>

	<div class="pushCart"></div>
  </div>
  <footer class="footer section text-center py-4 text-dark">
  <?php
  if(count($products) != 0) {
  ?>
  	<a href="<?php $path ?>/views/checkout.php" class="cartCheckout">Checkout</a>
  <?php
  }
  ?>
  </footer>

   <!-- CDN -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   
   <!-- Custom JS -->
   <script src="<?= $path ?>/assets/js/script.js"></script>
</body>
</html>
