<?php 
	$path = "..";

	function getContent() {
?>
	<section class="message section">
		<div class="message_container bd-grid">
			<div class="message_content">
				<div class="shop_message">
					<h4>Message Us</h4>

					<br>

					<p>Here in Sayugi, you will experience how to be special and we will make it all just for you.</p>

					<br>

					<p>Should you have any questions regarding our shop? please don't hesitate to drop us a message! </p>
				</div>
				<div class="message_form">
					<form>
						<input type="email" name="email" class="form-control fullInput" placeholder="EMAIL">
						<div class="form_halfInput">
							<input type="text" name="firstName" class="form-control halfInput" placeholder="First Name">
							<input type="text" name="lastName" class="form-control halfInput" placeholder="Last Name">
						</div>
						<input type="number" name="phone" class="form-control fullInput" placeholder="Phone Number">
						<textarea class="message_textarea form-control"placeholder="Enter your message here"></textarea>
						<div class="message_button">
							<button class="sendMessage">Send</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

<!-- ========= Authors Guarantee section ========= -->
	<section class="Guanratee guanratee_box">
		<div class="seller_guarantee bd-grid">
			<ul class="sellersMessage">
				<li class="sellersPromise">
					<i class="fas fa-gem"></i>
					<p>We are doing our best to deliver the <span>best quality</span> of item to your home</p>
				</li>
				<li class="sellersPromise">
					<i class="fas fa-truck"></i>
					<p><span>3-7 days ship nationwide</span> Cash on Delivery.</p>
				</li>
				<li class="sellersPromise">
					<i class="fas fa-shield-alt"></i>
					<p>
						<span>7-Day Return Guarantee</span>
							except from items like cakes and bread.
							Return damaged products without additional fees.
					</p>
				</li>
			</ul>
		</div>
	</section>

<?php
	}

	require_once $path . "/layouts/templates.php"
?>