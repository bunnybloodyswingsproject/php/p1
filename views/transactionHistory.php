<?php
   session_start();
   $path = "..";
   $transaction_path = ".";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Celigina</title>

	<!-- Metatags -->

	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">
   <meta charset="utf-8">
   <meta name="description" content="online shopping website for fashion clothes, customized cakes, and jewelry">
   <link rel="shortcut icon" type="image/png" href="../assets/images/sayugi5_logo7.png">

   <!-- Fontawesome -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <script src="https://use.fontawesome.com/1a8df02521.js"></script>
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

   <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&family=Potta+One&display=swap" rel="stylesheet">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

   <!-- Custom CSS -->
   <link rel="stylesheet" type="text/css" href="<?= $path ?>/assets/css/style.css">

   <!-- LightBox CSS -->
   <link rel="stylesheet" type="text/css" href="<?php $path ?>/assets/css/lightbox.min.css">

   <!--Lightbox JS -->
   <script type="text/javascript" src="<?php $path ?>/assets/js/lightbox-plus-jquery.min.js"></script>


</head>
<body>

	<div class="wrapper">
      <section class="transaction">
         <div class="transactionPage_container bd-grid">
            <div class="transaction_content">
               <div class="transaction_title">
                  <div class="transaction_logo">
                     <i class="fas fa-history"></i>
                     <h4>Transaction History</h4>
                  </div>
                  <div class="TransactReferer">
                     <a href="<?php $path ?>/views/home.php">
                        <i class="fas fa-times"></i>
                     </a>
                  </div>
               </div>
               <?php
               require_once "../controller/connection.php";

               $query = "SELECT orders.transaction_no as transaction_no,
               sum(products_orders.quantity * products_orders.price) as price FROM orders
               JOIN products_orders ON (orders.id = products_orders.order_id)
               WHERE user_id = {$_SESSION["user"]["id"]}
               GROUP BY transaction_no";

               $result = mysqli_query($link, $query);

               if(mysqli_num_rows($result) == 0) {
               ?>

               <div class="emptyCart">
                  <p>No orders to show</p>
                  <a href="<?php $path ?>/views/clothing.php" class="viewBtn">Go Shopping</a>
               </div>

               <?php
               }

               foreach($result as $index => $order) {
               ?>

               <div class="transaction_list">
                  <div class="transaction_code">
                     <div class="code_info">
                        <p class="code_index">1.</p>
                        <a href="<?php echo $path ?>/views/transaciton_details.php?transaction_id=<?= $order["transaction_no"] ?>"><?= $order["transaction_no"] ?></a>
                     </div>
                     <div class="transaction_price1">
                        <p class="text-danger">$<?= $order["price"] ?></p>
                     </div>
                  </div>
               </div>


               <?php
               }
               ?>
            </div>
         </div>
      </section>
   <div class="push"></div>
  </div>
  <footer class="footer section text-center py-4 text-dark">
   <small>&copy; 2021 SAYUJI</small>
  </footer>

   <!-- CDN -->
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   
   <!-- Custom JS -->
   <script src="<?= $path ?>/assets/js/script.js"></script>
</body>
</html>
