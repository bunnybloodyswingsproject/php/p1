<?php 
session_start();
$path = "..";

function getContent() {

require_once "../controller/connection.php";

$query = "
	SELECT 
			products.id as 'id',
			products.name as 'name',
			price,
			image_url
			FROM products JOIN categories ON (products.category_id = categories.id)
			WHERE products.category_id = 7
";

$results = mysqli_query($link, $query);

?>
	<!-- ========= Box Slider ========= -->
	<section class="box_slider">
		<div class="slide_container">
			<button class="btn left-btn hide">
				<i class="fas fa-angle-left"></i>
			</button>
			<div class="carousel-container">
				<ul class="carousel_section">
					<li class="slides activeSlide">
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/sayuji.png" class="story">
								<p>Our Name</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion10.jpg" class="has-story">
								<p>Baggy Jeans</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion11.jpg" class="has-story">
								<p>Spanish Style</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion12.jpg" class="has-story">
								<p>Spiral clothing</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion13.jpg" class="has-story">
								<p>Cool Jake</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion4.jpg" class="has-story">
								<p>Aurora Black</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion2.jpg" class="has-story">
								<p>Flouce Sleeve</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion5.jpg" class="has-story">
								<p>Suki Iwata</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion11.jpg" class="has-story">
								<p>Spanish Style</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion13.jpg" class="has-story">
								<p>Cool Jake</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion3.jpg" class="has-story">
								<p>Waist Cut</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion13.jpg" class="has-story">
								<p>Cool Jake</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion16.png" class="has-story">
								<p>Floral Style</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion15.jpg" class="has-story">
								<p>Semi Formal</p>
							</a>
						</div>
					</li>

					<li class="slides">
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion1.jpg" class="has-story">
								<p>Sweater</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion2.jpg" class="has-story">
								<p>Flouce Sleeve</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion3.jpg" class="has-story">
								<p>Waist Cut</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion11.jpg" class="has-story">
								<p>Spanish Style</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion4.jpg" class="has-story">
								<p>Aurora Black</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion5.jpg" class="has-story">
								<p>Suki Iwata</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion13.jpg" class="has-story">
								<p>Cool Jake</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion3.jpg" class="has-story">
								<p>Waist Cut</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion2.jpg" class="has-story">
								<p>Flouce Sleeve</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion2.jpg" class="has-story">
								<p>Flouce Sleeve</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion15.jpg" class="has-story">
								<p>Semi Formal</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion3.jpg" class="has-story">
								<p>Waist Cut</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion5.jpg" class="has-story">
								<p>Suki Iwata</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion16.png" class="has-story">
								<p>Floral Style</p>
							</a>
						</div>
					</li>

					<li class="slides">
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion14.jpg" class="has-story">
								<p>New York</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion3.jpg" class="has-story">
								<p>Waist Cut</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion15.jpg" class="has-story">
								<p>Semi Formal</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion16.png" class="has-story">
								<p>Floral Style</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion17.jpg" class="has-story">
								<p>Vermillion Queen</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion18.jpg" class="has-story">
								<p>Straight Slim</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion2.jpg" class="has-story">
								<p>Flouce Sleeve</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion5.jpg" class="has-story">
								<p>Suki Iwata</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion5.jpg" class="has-story">
								<p>Suki Iwata</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion16.png" class="has-story">
								<p>Floral Style</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion13.jpg" class="has-story">
								<p>Cool Jake</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion17.jpg" class="has-story">
								<p>Vermillion Queen</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/fashion11.jpg" class="has-story">
								<p>Spanish Style</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="">
								<img src="<?php $path ?>/assets/images/fashion17.jpg" class="has-story">
								<p>Vermillion Queen</p>
							</a>
						</div>
					</li>
				</ul>
			</div>
			<button class="btn right-btn">
				<i class="fas fa-angle-right nextButton"></i>
			</button>
		</div>	
	</section>

	<!-- ========= Item List For Clothing ========= -->
	<section class="clothing section">
		<div class="clothing_container bd-grid">
		<?php
			foreach ($results as $product) {
				if($product["image_url"] != "https://via.placeholder.com/150") {
					$product["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $product["image_url"];
				}
		?>

			<div class="box">
				<div class="slide-img">
					<img src="<?= $product["image_url"] ?>">
					<div class="overlay">
						<a href="<?php $path ?>/views/productDetails.php?product_id=<?= $product["id"] ?>" class="viewBtn">View Item</a>
					</div>
				</div>
				<div class="detail-box">
					<!-- ==== type ==== -->
					<div class="type">
						<a href="<?php $path ?>/views/productDetails.php?product_id=<?= $product["id"] ?>"><?= $product["name"] ?></a>
						<span>New Arrival</span>
					</div>
					<!-- ==== price ==== -->
					<a href="#" class="price">$<?= intval($product["price"]) ?></a>
				</div>
			</div>

		<?php
			}
		?>

		</div>
	</section>

	<!-- ========= NEWSLETTER Slider ========= -->
	<section class="newsletter section">
		<div class="newsletter_container bd-grid">
			<div class="newsletter_subscribe">
				<h2 class="sectionTitle">OUR NEWSLETTER</h2>
				<p class="newsletter_description">Promotion new products and sales. Directly to your</p>

				<form class="newsletter_form">
					<input type="text" name="" class="newsletter_input" placeholder="Enter your email">
					<a href="#" class="newsBtn">SUBSCRIBE</a>
				</form>
			</div>
		</div>
	</section>
<?php	
	}
?>

<?php require_once $path . "/layouts/templates.php" ?>