<?php
	session_start();
	$path = "..";
	require_once "../controller/connection.php";
	$transaction_id = $_GET["transaction_id"];

$query = "
			SELECT 
				orders.transaction_no as transaction_no,
			    orders.address as address,
			    orders.status as status,
			    orders.payment_method as payment_method,
			    orders.payment_status as payment_status,
			    orders.email as email,
			    orders.firstname as firstname,
			    orders.lastname as lastname,
			    orders.postalCode as postal_code,
			    orders.city as city,
			    orders.region as region,
			    orders.country as country,
			    orders.phone_number as contact_num,
			    products.name as product_name,
			    products.image_url as image_url,
			    products_orders.price,
			    products_orders.quantity
			    FROM orders
			    	JOIN products_orders ON (orders.id = products_orders.order_id)
			        JOIN products ON (products.id = products_orders.product_id)
			        WHERE transaction_no = {$transaction_id}
			        LIMIT 1
";

$getTransactionID = mysqli_query($link, $query);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Celigina</title>

	<!-- Metatags -->

	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">
	<meta charset="utf-8">
   	<meta name="description" content="online shopping website for fashion clothes, customized cakes, and jewelry">
	<link rel="shortcut icon" type="image/png" href="../assets/images/sayugi5_logo7.png">

   <!-- Fontawesome -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <script src="https://use.fontawesome.com/1a8df02521.js"></script>
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

   <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&family=Potta+One&display=swap" rel="stylesheet">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

   <!-- Custom CSS -->
   <link rel="stylesheet" type="text/css" href="<?= $path ?>/assets/css/style.css">

   <!-- LightBox CSS -->
   <link rel="stylesheet" type="text/css" href="<?php $path ?>/assets/css/lightbox.min.css">

   <!--Lightbox JS -->
   <script type="text/javascript" src="<?php $path ?>/assets/js/lightbox-plus-jquery.min.js"></script>


</head>
<body>

	<div class="wrapper">
		<div class="shopName">
			<h4>SAYUJI</h4>
		</div>

		<div class="transaction_container">
			<div class="transaction_box1">
				<section class="transaction_details">
					<div class="code_container bd-grid">
						<div class="code_content">
							<div class="code_greetings">
								<div class="checkLogo">
									<i class="fas fa-check"></i>
								</div>
								<?php 
								foreach($getTransactionID as $index => $getID) {
								?>

								<div class="code_here">
									<p>CODE: <?= $getID["transaction_no"] ?></p>
									<p class="thank_greetings">Thank you <?= $getID["firstname"] ?>!</p>
								</div>

							<?php
								}
							?>

							</div>

							<?php 

							$query = "
										SELECT 
											orders.transaction_no as transaction_no,
										    orders.address as address,
										    orders.status as status,
										    orders.payment_method as payment_method,
										    orders.payment_status as payment_status,
										    orders.email as email,
										    orders.firstname as firstname,
										    orders.lastname as lastname,
										    orders.postalCode as postal_code,
										    orders.city as city,
										    orders.region as region,
										    orders.country as country,
										    orders.phone_number as contact_num,
										    products.name as product_name,
										    products.image_url as image_url,
										    products_orders.price as price,
										    products_orders.quantity as quantity
										    FROM orders
										    	JOIN products_orders ON (orders.id = products_orders.order_id)
										        JOIN products ON (products.id = products_orders.product_id)
										        WHERE transaction_no = {$transaction_id}
							";

							$result = mysqli_query($link, $query);
							$subtotal = 0;
							$total = 0;
							$grandTotal = 0;
							$shippingFee = 200;

							foreach($result as $index => $order) {
								if($order["image_url"] != "https://via.placeholder.com/150"){
									$order["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $order["image_url"];
								}

								$subtotal = $order["quantity"] * $order["price"];
								$total += $subtotal;
								$grandTotal = $total + $shippingFee;
							?>

							<div class="transaction_list">
								<img src="<?= $order["image_url"] ?>">
								<div class="transaction_items">
									<p><?= $order["product_name"] ?></p>
									<p class="transaction_price"><?= $order["price"] ?></p>
									<p>Quantity: <?= $order["quantity"] ?></p>
								</div>
							</div>

							<?php
							}
							?>


							<div class="Transaction_totalCalculator">
								<div class="transacSubtotal">
									<div class="transacTotal">
										<p class="totalSubs">Subtotal</p>
										<p class="totalNumbers" id="subPriceTotal">$<?= $total ?></p>
									</div>
									<div class="transacTotal">
										<p class="totalSubs">Shipping Fee</p>
										<p class="totalNumbers" id="shippingFee">$<?= $shippingFee ?></p>
									</div>
								</div>
								<div class="TransactiongrandTotal">
									<div class="transacTotal">
										<p class="totalSubs">Grand Total</p>
										<p class="totalNumbers" id="grandTotal">$<?= $grandTotal ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="confirmation">
					<div class="confirmation_container bd-grid">
						<div class="confirmation_content">
							<h4>Your order is now confirmed</h4>
							<p>We’ve accepted your order, and we’re getting it ready. Come back to this page for updates on your shipment status.</p>
						</div>
					</div>
				</section>

				<section class="shipping">
					<div class="shipping_container bd-grid">
						<div class="shipping_content">
							<h4 class="shippingTitle">Shipping Schedule is now confirmed</h4>
							<div class="shipping_info">
								<div class="shipping_infoa1">
									<h4>Contact Information</h4>
									<p><?= $order["email"] ?></p>
								</div>
								<div class="shipping_infoa1">
									<h4>Payment Method</h4>
									<p>Cash on Delivery - <span>$<?= $shippingFee ?></span></p>
								</div>
							</div>

							<div class="shipping_info">
								<div class="shipping_infoa1">
									<h4>Shipping Address</h4>
									<p><?= $order["address"] ?></p>
									<p><?= $order["city"] ?></p>
									<p><?= $order["country"] ?></p>
									<p><?= $order["region"] ?></p>
									<p><?= $order["contact_num"] ?></p>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="important_notice sub_section">
					<div class="importantNote_container">
						<h4 id="notice">IMPORTANT:</h4>
						<div class="authorsNote_message">
							<p>HI Our Valued Customer,</p>
							<br>
							<p class="TransactionSentences">We, Sayuji, are doing our best to to give you the best service that we have in our shop. Due to the ongoing pandemic in our community, cancelation of the ordered products would be a hard process on our end.
							To avoid this process, kindly inform us on the same day of checkout of the product, so we can process this smoothly.</p>

							<br>

							<p class="sentences">Failure to do this would lead us to extent such as posting your personal information on all the social media platform such as facebook, instagram, and twitter etc.</p>

							<br>

							<p class="sentences">We hope we will not arrive on this kind of conclusion. We cared for your well being as much as our servce.</p>

							<br>

							<p>Thank you.</p>

							<br>

							<p>Sayuji</p>

						</div>
					</div>
				</section>
			</div>
			<div class="transaction_box2">
				<section class="important_notice sub_section">
					<div class="importantNote_container">
						<div class="authorsNote_message">
							<p>HI Our Valued Customer,</p>
							<br>
							<p class="TransactionSentences">I just wanted to take a moment of your time to thank you for continually choosing us. A recent look at the books proved that your guys are one of our most frequent (and loved) repeat customers. I’m looking forward to more of this in the future</p>

							<br>

							<p class="sentences">Also, for being such a great first-time buyer, here’s a little something extra: Use the code <span>SAYUJI15 to get 15% off</span> your next order with us.Thanks once again, looking forward to hearing your feedback!</p>

							<br>

							<p class="sentences">I’d love to hear your thoughts once you’ve got your hands on it!</p>

							<br>

							<p>Thank you.</p>

							<br>

							<p>Sayuji</p>

						</div>
					</div>
				</section>

				<?php 
				$query = "
						SELECT * FROM products WHERE products.category_id = 7
						LIMIT 3
				";

				$featured_clothe = mysqli_query($link, $query);
				?>
				
				<section class="featured_transaction">
					<div class="featuredTransac_container">
						<div class="feature_clothing">
							<h4>Fashion Of The Week</h4>
							<div class="featuredTransaction_box">
								<?php 
								foreach($featured_clothe as $clothe) {
									if($clothe["image_url"] != "https://via.placeholder.com/150"){
										$clothe["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $clothe["image_url"];
									}
								?>

								<div class="featuredcontent_list">
									<img src="<?= $clothe["image_url"] ?>">
									<div class="featuredContent_description">
										<p><?= $clothe["name"] ?></p>
										<p class="feature_price">$<?= intval($clothe["price"]) ?></p>
									</div>
								</div>
								<?php	
								}
								?>
							</div>
						</div>
					</div>
				</section>

				<?php 
				$query = "
						SELECT * FROM products WHERE products.category_id = 1
						LIMIT 3
				";

				$breads = mysqli_query($link, $query);
				?>

				<section class="featured_transaction">
					<div class="featuredTransac_container">
						<div class="feature_clothing">
							<h4>Pastry Of The Week</h4>
							<div class="featuredTransaction_box">
								<?php 
								foreach($breads as $bread) {
									if($bread["image_url"] != "https://via.placeholder.com/150"){
										$bread["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $bread["image_url"];
									}
								?>

								<div class="featuredcontent_list">
									<img src="<?= $bread["image_url"] ?>">
									<div class="featuredContent_description">
										<p><?= $bread["name"] ?></p>
										<p class="feature_price">$<?= intval($bread["price"]) ?></p>
									</div>
								</div>
								<?php	
								}
								?>
							</div>
						</div>
					</div>
				</section>

				<?php 
				$query = "
						SELECT * FROM products WHERE products.category_id = 10
						LIMIT 3
				";

				$jewelries = mysqli_query($link, $query);
				?>

				<section class="featured_transaction">
					<div class="featuredTransac_container">
						<div class="feature_clothing">
							<h4>Jewelry Of The Week</h4>
							<div class="featuredTransaction_box">
								<?php 
								foreach($jewelries as $jewelry) {
									if($jewelry["image_url"] != "https://via.placeholder.com/150"){
										$jewelry["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $jewelry["image_url"];
									}
								?>

								<div class="featuredcontent_list">
									<img src="<?= $jewelry["image_url"] ?>">
									<div class="featuredContent_description">
										<p><?= $jewelry["name"] ?></p>
										<p class="feature_price">$<?= intval($jewelry["price"]) ?></p>
									</div>
								</div>
								<?php	
								}
								?>
							</div>
						</div>
					</div>
				</section>


			</div>
		</div>

		<!-- ========= Authors Guarantee section ========= -->
		<section class="transactionGuanratee guanratee_box">
			<div class="seller_guarantee bd-grid">
				<ul class="sellersMessage">
					<li class="sellersPromiseTransaction">
						<i class="fas fa-gem"></i>
						<p>We are doing our best to deliver the <span>best quality</span> of item to your home</p>
					</li>
					<li class="sellersPromiseTransaction">
						<i class="fas fa-truck"></i>
						<p><span>3-7 days ship nationwide</span> Cash on Delivery.</p>
					</li>
					<li class="sellersPromiseTransaction">
						<i class="fas fa-shield-alt"></i>
						<p>
							<span>7-Day Return Guarantee</span>
							except from items like cakes and bread.
							Return damaged products without additional fees.
						</p>
					</li>
				</ul>
			</div>
		</section>

		<section class="goHome bd-grid">
			<div class="button_container">
				<p>Need help? <a href="<?php $path ?>/views/message.php" class="contact_us">Contact us</a></p>
				<a href="<?php $path ?>/views/clothing.php" class="goHomeBtn">SHOP AGAIN</a>
			</div>
		</section>

		<div class="push"></div>
  	</div>
	<footer class="footer section text-center py-4 text-dark">
		<small>&copy; 2021 SAYUJI</small>
	</footer>

   <!-- CDN -->
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   
   <!-- Custom JS -->
   <script src="<?= $path ?>/assets/js/script.js"></script>
</body>
</html>

