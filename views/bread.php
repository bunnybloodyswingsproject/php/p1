<?php 
	
	session_start();
	$path = "..";
	$storyPath = ".";

	function getContent() {
	require_once "../controller/connection.php";

	$query = "
	SELECT 
			products.id as 'id',
			products.name as 'name',
			price,
			image_url
			FROM products JOIN categories ON (products.category_id = categories.id)
			WHERE products.category_id = 1
";

$results = mysqli_query($link, $query);

?>

	<!-- ========= Box Slider ========= -->
	<section class="box_slider">
		<div class="slide_container">
			<button class="btn left-btn hide">
				<i class="fas fa-angle-left"></i>
			</button>
			<div class="carousel-container">
				<ul class="carousel_section">
					<li class="slides activeSlide">
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/sayuji.png" class="story">
								<p>Our Name</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread1.jpg" class="has-story">
								<p>Chow Pow</p>
							</a>
						</div>
						<div class="story-status">
							<a href="<?php $storyPath ?>/views/productDetails.php">
								<img src="<?php $path ?>/assets/images/bread2.jpg" class="has-story">
								<p>Spanish Bread</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread3.jpg" class="has-story">
								<p>Cheese Bread</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread8.jpg" class="has-story">
								<p>Strawberry Cream</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread4.jpg" class="has-story">
								<p>Turtle Bread</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread7.jpg" class="has-story">
								<p>Ensaymada</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread9.jpg" class="has-story">
								<p>Jalapeño</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread7.jpg" class="has-story">
								<p>Ensaymada</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread7.jpg" class="has-story">
								<p>Ensaymada</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread7.jpg" class="has-story">
								<p>Ensaymada</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread8.jpg" class="has-story">
								<p>Strawberry Cream</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread6.jpg" class="has-story">
								<p>Toasted Bread</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread8.jpg" class="has-story">
								<p>Strawberry Cream</p>
							</a>
						</div>
					</li>

					<li class="slides">
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread5.jpg" class="story">
								<p>Slice Bread</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread6.jpg" class="has-story">
								<p>Toasted Bread</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread7.jpg" class="has-story">
								<p>Ensaymada</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread8.jpg" class="has-story">
								<p>Strawberry Cream</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread9.jpg" class="has-story">
								<p>Jalapeño</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread3.jpg" class="has-story">
								<p>Cheese Bread</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread4.jpg" class="has-story">
								<p>Turtle Bread</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/bread12.jpg" class="has-story">
								<p>Sweet Roll</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread8.jpg" class="has-story">
								<p>Strawberry Cream</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread7.jpg" class="has-story">
								<p>Ensaymada</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread7.jpg" class="has-story">
								<p>Ensaymada</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread3.jpg" class="has-story">
								<p>Cheese Bread</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread4.jpg" class="has-story">
								<p>Turtle Bread</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/bread12.jpg" class="has-story">
								<p>Sweet Roll</p>
							</a>
						</div>
					</li>

					<li class="slides">
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/bread10.jpg" class="story">
								<p>Red Berry Deluxe</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/bread11.jpg" class="has-story">
								<p>Tuna BRead</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/bread12.jpg" class="has-story">
								<p>Sweet Roll</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/bread13.jpg" class="has-story">
								<p>Joe Doe</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/bread14.jpg" class="has-story">
								<p>The Making</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread7.jpg" class="has-story">
								<p>Ensaymada</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread4.jpg" class="has-story">
								<p>Turtle Bread</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/bread12.jpg" class="has-story">
								<p>Sweet Roll</p>
							</a>
						</div>
						<div class="story-status status-tab">
							<a href="">
								<img src="<?php $path ?>/assets/images/bread12.jpg" class="has-story">
								<p>Sweet Roll</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/sayuji.png" class="story">
								<p>Our Name</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread1.jpg" class="has-story">
								<p>Chow Pow</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread6.jpg" class="has-story">
								<p>Toasted Bread</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread6.jpg" class="has-story">
								<p>Toasted Bread</p>
							</a>
						</div>
						<div class="story-status status-tab status-tab2">
							<a href="#">
								<img src="<?php $path ?>/assets/images/bread7.jpg" class="has-story">
								<p>Ensaymada</p>
							</a>
						</div>
					</li>
				</ul>
			</div>
			<button class="btn right-btn">
				<i class="fas fa-angle-right nextButton"></i>
			</button>
		</div>	
	</section>

	<!-- ========= Item List For Clothing ========= -->
	<section class="clothing section">
		<div class="clothing_container bd-grid">
			<?php 
				foreach ($results as $product) {
					if($product["image_url"] != "https://via.placeholder.com/150") {
						$product["image_url"] = "http://".$_SERVER["HTTP_HOST"]."/assets/images/" . $product["image_url"];
				}
			?>
				<div class="box">
					<div class="slide-img">
						<img src="<?= $product["image_url"] ?>">
						<div class="overlay">
							<a href="<?php $path ?>/views/productDetails.php?product_id=<?= $product["id"] ?>" class="viewBtn">View Item</a>
						</div>
					</div>
					<div class="detail-box">
						<!-- ==== type ==== -->
						<div class="type">
							<a href="<?php $path ?>/views/productDetails.php?product_id=<?= $product["id"] ?>"><?= $product["name"] ?></a>
							<span>New Arrival</span>
						</div>
						<!-- ==== price ==== -->
						<a href="#" class="price">$<?= intval($product["price"]) ?></a>
					</div>
				</div>

			<?php
				}
			?>

		</div>
	</section>

	<!-- ========= NEWSLETTER Slider ========= -->
	<section class="newsletter section">
		<div class="newsletter_container bd-grid">
			<div class="newsletter_subscribe">
				<h2 class="sectionTitle">OUR NEWSLETTER</h2>
				<p class="newsletter_description">Promotion new products and sales. Directly to your</p>

				<form class="newsletter_form">
					<input type="text" name="" class="newsletter_input" placeholder="Enter your email">
					<a href="#" class="newsBtn">SUBSCRIBE</a>
				</form>
			</div>
		</div>
	</section>

<?php 
	}

	require_once $path . "/layouts/templates.php";
?>
