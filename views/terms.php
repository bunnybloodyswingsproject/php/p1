<!DOCTYPE html>
<html>
<head>
	<title>Celigina</title>

	<!-- Metatags -->

	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">
  <meta charset="utf-8">
  <meta name="description" content="online shopping website for fashion clothes, customized cakes, and jewelry">
  <link rel="shortcut icon" type="image/png" href="../assets/images/sayugi5_logo7.png">

   <!-- Fontawesome -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <script src="https://use.fontawesome.com/1a8df02521.js"></script>
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

   <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&family=Potta+One&display=swap" rel="stylesheet">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

   <!-- Custom CSS -->
   <link rel="stylesheet" type="text/css" href="<?= $path ?>/assets/css/style.css">

   <!-- LightBox CSS -->
   <link rel="stylesheet" type="text/css" href="<?php $path ?>/assets/css/lightbox.min.css">

   <!--Lightbox JS -->
   <script type="text/javascript" src="<?php $path ?>/assets/js/lightbox-plus-jquery.min.js"></script>


</head>
<body>

	<div class="wrapper">

   <div class="push"></div>
  </div>
  <footer class="footer section text-center py-4 text-dark">
    <div class="footer_container bd_grid">
      <div class="footer_box">
        <h3 class="footer_title">Main Menu</h3>
        <i class="fas fa-caret-down dropDownBtn 1"></i>
        <ul class="dropDownList">
          <li><a href="<?php $path ?>/views/home.php" class="footer_link">HOME</a></li>
          <li class="footer_link"><a href="<?php $path ?>/views/clothing.php">CLOTHING</a></li>
          <li class="footer_link"><a href="<?php $path ?>/views/bread.php">BAKERY</a></li>
          <li class="footer_link"><a href="<?php $path ?>/views/jewelry.php">JEWELRY</a></li>
        </ul>
      </div>

      <div class="footer_box">
        <h3 class="footer_title">Services</h3>
        <i class="fas fa-caret-down dropDownBtn 2"></i>
          <ul class="dropDownList">
            <li class="#" class="footer_link"><a href="<?php $path ?>/views/message.php">Free Shipping</a></li>
            <li class="#" class="footer_link"><a href="<?php $path ?>/views/message.php">Special Discount</a></li>
          </ul>
      </div>

      <div class="footer_box">
        <h3 class="footer_title">Social Media Links</h3>
        <i class="fas fa-caret-down dropDownBtn 3"></i>
          <ul class="socialMediaLinks dropDownList">
            <li>
              <a href="https://www.facebook.com/cegisboutique/" class="footer_social"><i class="fab fa-facebook-square"></i>
            </li>
            <li>
              <a href="#" class="footer_social"><i class="fab fa-twitter-square"></i></a>
            </li>
            <li>
              <a href="#" class="footer_social"><i class="fab fa-google"></i></a>
            </li>
          </ul>
      </div>
    </div>
   <small>&copy; 2021 SAYUJI</small>
  </footer>

   <!-- CDN -->
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   
   <!-- Custom JS -->
   <script src="<?= $path ?>/assets/js/script.js"></script>
</body>
</html>
