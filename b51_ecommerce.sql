-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 07, 2020 at 03:36 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `b51_ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Cooking'),
(2, 'Drinks'),
(3, 'Canned Goods'),
(4, 'Toiletries'),
(5, 'Meat'),
(6, 'Vegetables'),
(7, 'Office Supplies'),
(8, 'Finger Foods'),
(9, 'Infant Supplies'),
(10, 'Medical Supplies');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `purchase_date` timestamp NULL DEFAULT current_timestamp(),
  `transaction_no` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'pending',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `purchase_date`, `transaction_no`, `user_id`, `address`, `status`, `payment_method`, `payment_status`) VALUES
(1, '2020-07-03 00:00:00', '2020731', 4, 'ABCD EEF G', 'delivered', 'cod', 'paid'),
(2, '2020-07-07 12:31:38', '202007061', 1, '', 'cancelled', 'cod', 'pending'),
(4, '2020-07-07 12:31:42', '202007064', 1, '', 'cancelled', 'cod', 'pending'),
(5, '2020-07-07 12:35:02', '202007065', 1, '', 'cancelled', 'cod', 'pending'),
(6, '2020-07-06 10:52:00', '202007066', 1, '', 'cancelled', 'cod', 'pending'),
(7, '2020-07-06 10:57:44', '202007067', 1, '', 'pending', 'cod', 'pending'),
(8, '2020-07-06 11:11:22', '202007068', 1, '', 'pending', 'cod', 'pending'),
(9, '2020-07-06 11:27:57', '202007069', 1, '', 'pending', 'cod', 'pending'),
(10, '2020-07-06 12:06:27', '2020070610', 1, '', 'cancelled', 'cod', 'pending'),
(11, '2020-07-06 12:08:48', '2020070611', 1, '', 'pending', 'cod', 'pending'),
(12, '2020-07-06 12:16:23', '2020070612', 1, 'Here', 'pending', 'cod', 'pending'),
(13, '2020-07-06 12:18:09', '2020070613', 1, '', 'pending', 'cod', 'pending'),
(14, '2020-07-06 13:26:41', '2020070614', 1, 'asdf', 'pending', 'cod', 'pending'),
(15, '2020-07-07 10:59:53', '2020070715', 1, '123', 'pending', 'cod', 'pending'),
(16, '2020-07-07 11:16:07', '2020070716', 1, '1234 Street', 'pending', 'cod', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(18,4) NOT NULL DEFAULT 0.0000,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `image_url` varchar(255) DEFAULT 'https://via.placeholder.com/150'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `category_id`, `image_url`) VALUES
(1, 'Mousse - Banana Chocolate', '3.0000', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 6, 'https://via.placeholder.com/150'),
(2, 'Kohlrabi', '24.0000', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 10, 'https://via.placeholder.com/150'),
(3, 'Juice - Apple, 500 Ml', '73.0000', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 2, 'https://via.placeholder.com/150'),
(4, 'Onions Granulated', '2.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 7, 'https://via.placeholder.com/150'),
(5, 'Shopper Bag - S - 4', '75.0000', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 1, 'https://via.placeholder.com/150'),
(6, 'Lettuce - Frisee', '80.0000', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 6, 'https://via.placeholder.com/150'),
(7, 'Red Pepper Paste', '65.0000', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 3, 'https://via.placeholder.com/150'),
(8, 'Sausage - Meat', '54.0000', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 4, 'https://via.placeholder.com/150'),
(9, 'Plate Foam Laminated 9in Blk', '56.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 3, 'https://via.placeholder.com/150'),
(10, 'Bread - Crusty Italian Poly', '69.0000', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 7, 'https://via.placeholder.com/150'),
(11, 'Water - Perrier', '68.0000', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 8, 'https://via.placeholder.com/150'),
(12, 'Nantucket - Orange Mango Cktl', '70.0000', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 6, 'https://via.placeholder.com/150'),
(13, 'Crush - Orange, 355ml', '95.0000', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 3, 'https://via.placeholder.com/150'),
(14, 'Hand Towel', '20.0000', 'In congue. Etiam justo. Etiam pretium iaculis justo.', 8, 'https://via.placeholder.com/150'),
(15, 'External Supplier', '77.0000', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 9, 'https://via.placeholder.com/150'),
(16, 'Wine - Magnotta - Bel Paese White', '28.0000', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 4, 'https://via.placeholder.com/150'),
(17, 'Mace', '97.0000', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 3, 'https://via.placeholder.com/150'),
(18, 'Bagel - Whole White Sesame', '26.0000', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 9, 'https://via.placeholder.com/150'),
(19, 'Cheese - Brie', '5.0000', 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 5, 'https://via.placeholder.com/150'),
(20, 'Grenadine', '65.0000', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 1, 'https://via.placeholder.com/150'),
(21, 'Quail - Jumbo', '60.0000', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 6, 'https://via.placeholder.com/150'),
(22, 'Water - Spring Water 500ml', '59.0000', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 1, 'https://via.placeholder.com/150'),
(23, 'Bacardi Mojito', '47.0000', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 9, 'https://via.placeholder.com/150'),
(24, 'Blackberries', '69.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 2, 'https://via.placeholder.com/150'),
(25, 'Lamb - Shoulder', '83.0000', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 5, 'https://via.placeholder.com/150'),
(26, 'Trout Rainbow Whole', '46.0000', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 1, 'https://via.placeholder.com/150'),
(27, 'Wine - Alsace Gewurztraminer', '98.0000', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 6, 'https://via.placeholder.com/150'),
(28, 'Beets - Golden', '70.0000', 'Fusce consequat. Nulla nisl. Nunc nisl.', 2, 'https://via.placeholder.com/150'),
(29, 'Beef - Rib Roast, Capless', '14.0000', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 2, 'https://via.placeholder.com/150'),
(30, 'Beef - Tenderloin - Aa', '35.0000', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 3, 'https://via.placeholder.com/150'),
(31, 'Sunflower Seed Raw', '27.0000', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 3, 'https://via.placeholder.com/150'),
(32, 'Fond - Neutral', '31.0000', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 7, 'https://via.placeholder.com/150'),
(33, 'Kiwi', '90.0000', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 3, 'https://via.placeholder.com/150'),
(34, 'Mousse - Mango', '58.0000', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 10, 'https://via.placeholder.com/150'),
(35, 'Pickerel - Fillets', '59.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 4, 'https://via.placeholder.com/150'),
(36, 'Longos - Chicken Cordon Bleu', '49.0000', 'In congue. Etiam justo. Etiam pretium iaculis justo.', 3, 'https://via.placeholder.com/150'),
(37, 'Oregano - Dry, Rubbed', '48.0000', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 4, 'https://via.placeholder.com/150'),
(38, 'Wine - Chablis J Moreau Et Fils', '5.0000', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 10, 'https://via.placeholder.com/150'),
(39, 'Sauce - Thousand Island', '37.0000', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 5, 'https://via.placeholder.com/150'),
(40, 'Lamb - Leg, Diced', '95.0000', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.', 5, 'https://via.placeholder.com/150'),
(41, 'Juice - Orange, Concentrate', '39.0000', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 6, 'https://via.placeholder.com/150'),
(42, 'Beets - Mini Golden', '97.0000', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 5, 'https://via.placeholder.com/150'),
(43, 'Sage - Ground', '64.0000', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 4, 'https://via.placeholder.com/150'),
(44, 'Pheasants - Whole', '10.0000', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 6, 'https://via.placeholder.com/150'),
(45, 'Tuna - Loin', '86.0000', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 3, 'https://via.placeholder.com/150'),
(46, 'Juice - Apple, 1.36l', '84.0000', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', 7, 'https://via.placeholder.com/150'),
(47, 'Wine - Malbec Trapiche Reserve', '10.0000', 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 7, 'https://via.placeholder.com/150'),
(48, 'Mahi Mahi', '15.0000', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 10, 'https://via.placeholder.com/150'),
(49, 'Table Cloth 91x91 Colour', '9.0000', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 9, 'https://via.placeholder.com/150'),
(50, 'Mushroom - Crimini', '18.0000', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 8, 'https://via.placeholder.com/150'),
(51, 'Shichimi Togarashi Peppeers', '99.0000', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 9, 'https://via.placeholder.com/150'),
(52, 'Liners - Baking Cups', '12.0000', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 7, 'https://via.placeholder.com/150'),
(53, 'Pail - 4l White, With Handle', '6.0000', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 9, 'https://via.placeholder.com/150'),
(54, 'Cheese - Havarti, Salsa', '41.0000', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 6, 'https://via.placeholder.com/150'),
(55, 'Wine - Casillero Deldiablo', '60.0000', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 9, 'https://via.placeholder.com/150'),
(56, 'Bay Leaf', '15.0000', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 7, 'https://via.placeholder.com/150'),
(57, 'Table Cloth 54x54 White', '95.0000', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 2, 'https://via.placeholder.com/150'),
(58, 'Split Peas - Yellow, Dry', '94.0000', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 9, 'https://via.placeholder.com/150'),
(59, 'Beef - Top Sirloin', '69.0000', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 4, 'https://via.placeholder.com/150'),
(60, 'Ecolab - Balanced Fusion', '48.0000', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 2, 'https://via.placeholder.com/150'),
(61, 'Apron', '21.0000', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 3, 'https://via.placeholder.com/150'),
(62, 'Cherries - Bing, Canned', '26.0000', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', 8, 'https://via.placeholder.com/150'),
(63, 'Lamb - Shoulder', '51.0000', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 10, 'https://via.placeholder.com/150'),
(64, 'Truffle - Peelings', '30.0000', 'Fusce consequat. Nulla nisl. Nunc nisl.', 1, 'https://via.placeholder.com/150'),
(65, 'Bread - Petit Baguette', '65.0000', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 8, 'https://via.placeholder.com/150'),
(66, 'Wine - Champagne Brut Veuve', '38.0000', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 3, 'https://via.placeholder.com/150'),
(67, 'Foam Cup 6 Oz', '52.0000', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 1, 'https://via.placeholder.com/150'),
(68, 'Beef - Bresaola', '46.0000', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 9, 'https://via.placeholder.com/150'),
(69, 'Skewers - Bamboo', '37.0000', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 8, 'https://via.placeholder.com/150'),
(70, 'Bag - Bread, White, Plain', '84.0000', 'Fusce consequat. Nulla nisl. Nunc nisl.', 2, 'https://via.placeholder.com/150'),
(71, 'Chicken - White Meat With Tender', '40.0000', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 1, 'https://via.placeholder.com/150'),
(72, 'Juice - Happy Planet', '45.0000', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', 1, 'https://via.placeholder.com/150'),
(73, 'Anisette - Mcguiness', '82.0000', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 4, 'https://via.placeholder.com/150'),
(74, 'Chicken - Tenderloin', '31.0000', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 4, 'https://via.placeholder.com/150'),
(75, 'General Purpose Trigger', '89.0000', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 9, 'https://via.placeholder.com/150'),
(76, 'Rolled Oats', '28.0000', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 10, 'https://via.placeholder.com/150'),
(77, 'Steampan Lid', '100.0000', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 6, 'https://via.placeholder.com/150'),
(78, 'Glaze - Clear', '36.0000', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 9, 'https://via.placeholder.com/150'),
(79, 'Bacardi Breezer - Strawberry', '33.0000', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 6, 'https://via.placeholder.com/150'),
(80, 'Chinese Foods - Pepper Beef', '75.0000', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.', 2, 'https://via.placeholder.com/150'),
(81, 'Chinese Foods - Chicken', '52.0000', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 2, 'https://via.placeholder.com/150'),
(82, 'Sprouts - Brussel', '91.0000', 'Fusce consequat. Nulla nisl. Nunc nisl.', 7, 'https://via.placeholder.com/150'),
(83, 'Beer - Creemore', '4.0000', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 1, 'https://via.placeholder.com/150'),
(84, 'Dikon', '29.0000', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 8, 'https://via.placeholder.com/150'),
(85, 'Filter - Coffee', '93.0000', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 1, 'https://via.placeholder.com/150'),
(86, 'Compound - Strawberry', '29.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 3, 'https://via.placeholder.com/150'),
(87, 'Beef - Ox Tongue, Pickled', '40.0000', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 2, 'https://via.placeholder.com/150'),
(88, 'Bagel - Plain', '64.0000', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 1, 'https://via.placeholder.com/150'),
(89, 'Pastry - Trippleberry Muffin - Mini', '77.0000', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 1, 'https://via.placeholder.com/150'),
(90, 'Saskatoon Berries - Frozen', '44.0000', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 2, 'https://via.placeholder.com/150'),
(91, 'Basil - Thai', '12.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 6, 'https://via.placeholder.com/150'),
(92, 'Cheese - Mozzarella', '97.0000', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 3, 'https://via.placeholder.com/150'),
(93, 'Soup - Campbells, Chix Gumbo', '89.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 4, 'https://via.placeholder.com/150'),
(94, 'Fruit Salad Deluxe', '19.0000', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 4, 'https://via.placeholder.com/150'),
(95, 'Bread - White Epi Baguette', '94.0000', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 8, 'https://via.placeholder.com/150'),
(96, 'Honey - Comb', '22.0000', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 5, 'https://via.placeholder.com/150'),
(97, 'Bread - Mini Hamburger Bun', '69.0000', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 5, 'https://via.placeholder.com/150'),
(98, 'Fenngreek Seed', '66.0000', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 9, 'https://via.placeholder.com/150'),
(99, 'Pail With Metal Handle 16l White', '49.0000', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 4, 'https://via.placeholder.com/150'),
(100, 'Transfer Sheets', '7.0000', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 1, 'https://via.placeholder.com/150'),
(101, 'Pork - European Side Bacon', '14.0000', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 7, 'https://via.placeholder.com/150'),
(102, 'Flower - Commercial Bronze', '37.0000', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 8, 'https://via.placeholder.com/150'),
(103, 'Carrots - Jumbo', '82.0000', 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 10, 'https://via.placeholder.com/150'),
(104, 'Worcestershire Sauce', '17.0000', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 7, 'https://via.placeholder.com/150'),
(105, 'Wine - Casablanca Valley', '57.0000', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 1, 'https://via.placeholder.com/150'),
(106, 'Coconut - Whole', '85.0000', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 10, 'https://via.placeholder.com/150'),
(107, 'Coriander - Seed', '69.0000', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 5, 'https://via.placeholder.com/150'),
(108, 'Cinnamon Buns Sticky', '12.0000', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', 4, 'https://via.placeholder.com/150'),
(109, 'Cucumber - English', '44.0000', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 10, 'https://via.placeholder.com/150'),
(110, 'V8 Pet', '96.0000', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 5, 'https://via.placeholder.com/150'),
(111, 'Table Cloth - 53x69 Colour', '92.0000', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 9, 'https://via.placeholder.com/150'),
(112, 'Butter Ripple - Phillips', '4.0000', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 1, 'https://via.placeholder.com/150'),
(113, 'Bread Bowl Plain', '57.0000', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 6, 'https://via.placeholder.com/150'),
(114, 'Gatorade - Orange', '38.0000', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 7, 'https://via.placeholder.com/150'),
(115, 'Lamb - Bones', '18.0000', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 1, 'https://via.placeholder.com/150'),
(116, 'Pie Pecan', '92.0000', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', 3, 'https://via.placeholder.com/150'),
(117, 'Rye Special Old', '74.0000', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 1, 'https://via.placeholder.com/150'),
(118, 'Spinach - Baby', '78.0000', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 4, 'https://via.placeholder.com/150'),
(119, 'Table Cloth 120 Round White', '12.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 6, 'https://via.placeholder.com/150'),
(120, 'Paper Cocktail Umberlla 80 - 180', '18.0000', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 8, 'https://via.placeholder.com/150'),
(121, 'Napkin - Dinner, White', '59.0000', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 8, 'https://via.placeholder.com/150'),
(122, 'Seedlings - Mix, Organic', '24.0000', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 2, 'https://via.placeholder.com/150'),
(123, 'Pasta - Penne Primavera, Single', '7.0000', 'Fusce consequat. Nulla nisl. Nunc nisl.', 9, 'https://via.placeholder.com/150'),
(124, 'Calypso - Pineapple Passion', '7.0000', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 1, 'https://via.placeholder.com/150'),
(125, 'Crab - Back Fin Meat, Canned', '11.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 2, 'https://via.placeholder.com/150'),
(126, 'Momiji Oroshi Chili Sauce', '62.0000', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 4, 'https://via.placeholder.com/150'),
(127, 'Chocolate Bar - Reese Pieces', '60.0000', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 10, 'https://via.placeholder.com/150'),
(128, 'True - Vue Containers', '53.0000', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 2, 'https://via.placeholder.com/150'),
(129, 'Cup Translucent 9 Oz', '18.0000', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 10, 'https://via.placeholder.com/150'),
(130, 'Honey - Liquid', '60.0000', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 7, 'https://via.placeholder.com/150'),
(131, 'Soup Campbells - Tomato Bisque', '48.0000', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 3, 'https://via.placeholder.com/150'),
(132, 'Chicken - Thigh, Bone In', '63.0000', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', 8, 'https://via.placeholder.com/150'),
(133, 'Artichokes - Knobless, White', '69.0000', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 8, 'https://via.placeholder.com/150'),
(134, 'Paste - Black Olive', '55.0000', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 1, 'https://via.placeholder.com/150'),
(135, 'Danishes - Mini Cheese', '95.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 6, 'https://via.placeholder.com/150'),
(136, 'Pants Custom Dry Clean', '69.0000', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 6, 'https://via.placeholder.com/150'),
(137, 'Nantucket Cranberry Juice', '97.0000', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 5, 'https://via.placeholder.com/150'),
(138, 'Lychee', '26.0000', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 7, 'https://via.placeholder.com/150'),
(139, 'Tea - Camomele', '51.0000', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 1, 'https://via.placeholder.com/150'),
(140, 'Bagels Poppyseed', '77.0000', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 5, 'https://via.placeholder.com/150'),
(141, 'Nut - Chestnuts, Whole', '79.0000', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 1, 'https://via.placeholder.com/150'),
(142, 'Veal - Osso Bucco', '16.0000', 'In congue. Etiam justo. Etiam pretium iaculis justo.', 8, 'https://via.placeholder.com/150'),
(143, 'Chicken - Whole Fryers', '74.0000', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 10, 'https://via.placeholder.com/150'),
(144, 'Persimmons', '88.0000', 'Fusce consequat. Nulla nisl. Nunc nisl.', 10, 'https://via.placeholder.com/150'),
(145, 'Cheese - Ermite Bleu', '31.0000', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 7, 'https://via.placeholder.com/150'),
(146, 'Apples - Spartan', '42.0000', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 6, 'https://via.placeholder.com/150'),
(147, 'Oil - Pumpkinseed', '3.0000', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 1, 'https://via.placeholder.com/150'),
(148, 'Bread - Triangle White', '5.0000', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 5, 'https://via.placeholder.com/150'),
(149, 'Honey - Lavender', '98.0000', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 5, 'https://via.placeholder.com/150'),
(150, 'Nut - Pistachio, Shelled', '93.0000', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 6, 'https://via.placeholder.com/150'),
(151, 'Chocolate Liqueur - Godet White', '31.0000', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 8, 'https://via.placeholder.com/150'),
(152, 'Steampan - Lid For Half Size', '95.0000', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 1, 'https://via.placeholder.com/150'),
(153, 'Honey - Lavender', '57.0000', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 5, 'https://via.placeholder.com/150'),
(154, 'Shallots', '95.0000', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 5, 'https://via.placeholder.com/150'),
(155, 'Butter - Unsalted', '15.0000', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 7, 'https://via.placeholder.com/150'),
(156, 'Oil - Peanut', '90.0000', 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 2, 'https://via.placeholder.com/150'),
(157, 'Salami - Genova', '17.0000', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 3, 'https://via.placeholder.com/150'),
(158, 'Daikon Radish', '35.0000', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 3, 'https://via.placeholder.com/150'),
(159, 'Tart - Lemon', '16.0000', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 8, 'https://via.placeholder.com/150'),
(160, 'Blue Curacao - Marie Brizard', '10.0000', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 6, 'https://via.placeholder.com/150'),
(161, 'Pear - Packum', '44.0000', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 9, 'https://via.placeholder.com/150'),
(162, 'V8 - Berry Blend', '7.0000', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 10, 'https://via.placeholder.com/150'),
(163, 'Syrup - Pancake', '88.0000', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 7, 'https://via.placeholder.com/150'),
(164, 'Placemat - Scallop, White', '86.0000', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 6, 'https://via.placeholder.com/150'),
(165, 'Ham - Smoked, Bone - In', '20.0000', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 6, 'https://via.placeholder.com/150'),
(166, 'Wine - White, Mosel Gold', '12.0000', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 10, 'https://via.placeholder.com/150'),
(167, 'Wine - Muscadet Sur Lie', '18.0000', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 7, 'https://via.placeholder.com/150'),
(168, 'Bread - Raisin', '30.0000', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 7, 'https://via.placeholder.com/150'),
(169, 'Table Cloth 62x120 White', '50.0000', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 6, 'https://via.placeholder.com/150'),
(170, 'Cake - Box Window 10x10x2.5', '51.0000', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 9, 'https://via.placeholder.com/150'),
(171, 'Daikon Radish', '54.0000', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 8, 'https://via.placeholder.com/150'),
(172, 'Sausage - Liver', '87.0000', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 4, 'https://via.placeholder.com/150'),
(173, 'Beans - Long, Chinese', '92.0000', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 7, 'https://via.placeholder.com/150'),
(174, 'Coffee - Flavoured', '76.0000', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 5, 'https://via.placeholder.com/150'),
(175, 'Wine - Shiraz Wolf Blass Premium', '22.0000', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 4, 'https://via.placeholder.com/150'),
(176, 'Wine - Pinot Noir Latour', '79.0000', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 3, 'https://via.placeholder.com/150'),
(177, 'Bag Stand', '99.0000', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 10, 'https://via.placeholder.com/150'),
(178, 'Steam Pan Full Lid', '28.0000', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 1, 'https://via.placeholder.com/150'),
(179, 'Island Oasis - Magarita Mix', '53.0000', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 1, 'https://via.placeholder.com/150'),
(180, 'Gelatine Powder', '49.0000', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 2, 'https://via.placeholder.com/150'),
(181, 'Easy Off Oven Cleaner', '21.0000', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 6, 'https://via.placeholder.com/150'),
(182, 'Bread - Multigrain', '25.0000', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 6, 'https://via.placeholder.com/150'),
(183, 'Beans - Black Bean, Canned', '88.0000', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 1, 'https://via.placeholder.com/150'),
(184, 'Pasta - Penne Primavera, Single', '94.0000', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 1, 'https://via.placeholder.com/150'),
(185, 'Tomatoes - Orange', '32.0000', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 8, 'https://via.placeholder.com/150'),
(186, 'Veal - Liver', '52.0000', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 5, 'https://via.placeholder.com/150'),
(187, 'Soup - French Can Pea', '61.0000', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 1, 'https://via.placeholder.com/150'),
(188, 'Dikon', '64.0000', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 6, 'https://via.placeholder.com/150'),
(189, 'Bay Leaf Ground', '5.0000', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 5, 'https://via.placeholder.com/150'),
(190, 'Bagelers - Cinn / Brown', '86.0000', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 4, 'https://via.placeholder.com/150'),
(191, 'Muffin - Bran Ind Wrpd', '60.0000', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 1, 'https://via.placeholder.com/150'),
(192, 'Lamb - Shoulder, Boneless', '43.0000', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 5, 'https://via.placeholder.com/150'),
(193, 'Spice - Montreal Steak Spice', '89.0000', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 10, 'https://via.placeholder.com/150'),
(194, 'Pastry - Mini French Pastries', '98.0000', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 10, 'https://via.placeholder.com/150'),
(195, 'Propel Sport Drink', '49.0000', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 4, 'https://via.placeholder.com/150'),
(196, 'Lid - 3oz Med Rec', '32.0000', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', 1, 'https://via.placeholder.com/150'),
(197, 'Broccoli - Fresh', '14.0000', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 6, 'https://via.placeholder.com/150'),
(198, 'Lid Coffeecup 12oz D9542b', '28.0000', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 7, 'https://via.placeholder.com/150'),
(199, 'Wine - Casillero Del Diablo', '51.0000', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.', 7, 'https://via.placeholder.com/150'),
(200, 'Pastry - Key Limepoppy Seed Tea', '19.0000', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 3, 'https://via.placeholder.com/150');

-- --------------------------------------------------------

--
-- Table structure for table `products_orders`
--

CREATE TABLE `products_orders` (
  `id` int(11) NOT NULL,
  `price` decimal(18,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_orders`
--

INSERT INTO `products_orders` (`id`, `price`, `quantity`, `product_id`, `order_id`) VALUES
(1, '75.00', 1, 5, 8),
(2, '65.00', 1, 20, 8),
(3, '59.00', 1, 22, 8),
(4, '46.00', 2, 26, 11),
(5, '30.00', 1, 64, 11),
(6, '40.00', 3, 71, 12),
(7, '45.00', 3, 72, 12),
(8, '77.00', 3, 89, 13),
(9, '7.00', 5, 100, 13),
(10, '75.00', 1, 5, 14),
(11, '75.00', 2, 5, 15),
(12, '75.00', 2, 5, 16);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `firstname`, `lastname`) VALUES
(1, 'j.mason@gmail.com', '$2y$10$.JlAPFsr2hWBZuQiCHmub.dK5dWrOE9XkTD1o8nGsZyd7dPqqiQni', 'Josh', 'Mason'),
(2, 'c.malig@gmail.com', '$2y$10$PtjtHiDbjOQy9kbB00tEIOAqPDjAGNlFZDCMGFdCj4jixnTWX2NBG', 'Clocke', 'Malig'),
(3, 'r.romero@gmail.com', '$2y$10$6Rx8dRZ1CAvSiUc0k03ApOImBVvqq0SwaiLbZ39peecyBoP1lSOz2', 'Ralph', 'Romero'),
(4, 'renz.martin@gmail.com', '$2y$10$tFjbvPhc6HZoRJc9mkUFL..0gYhMjfRcSw4gvvnS5xnqXaepFWv0e', 'renz', 'martin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transaction_no` (`transaction_no`),
  ADD KEY `orders_fk0` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_fk0` (`category_id`);

--
-- Indexes for table `products_orders`
--
ALTER TABLE `products_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_orders_fk0` (`product_id`),
  ADD KEY `products_orders_fk1` (`order_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `products_orders`
--
ALTER TABLE `products_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_fk0` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_fk0` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `products_orders`
--
ALTER TABLE `products_orders`
  ADD CONSTRAINT `products_orders_fk0` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `products_orders_fk1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
