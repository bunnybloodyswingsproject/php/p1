const sectionOne = document.querySelector(".main-1");
const sections = document.querySelectorAll("section");
const box_sliders = document.querySelectorAll(".sliding");
/*
const options = {
	root: null, //it is the viewport
	threshold: 0,
	rootMargin: "-150px"
};
const observer = new IntersectionObserver(function(entries, observer){
	entries.forEach(entry => {
		if(!entry.isIntersecting) {
			return;
		}
		console.log(entry.target);
		entry.target.classList.toggle('inverse');
	});
}, options);

sections.forEach(section => {
	observer.observe(section);
});
*/

const appearOptions = {
	threshold: 0,
	rootMargin: "0px 0px -250px 0px"
};

//Fading Effects
const faders = document.querySelectorAll(".fade-In");
const appearOnScroll = new IntersectionObserver(function(entries, appearOnScroll) {
	entries.forEach(entry => {
		if(!entry.isIntersecting) {
			return;
		}else{
			entry.target.classList.toggle("appear");
			appearOnScroll.unobserve(entry.target);
		}
	});
}, appearOptions);

faders.forEach(fader => {
	appearOnScroll.observe(fader);
});

box_sliders.forEach(slider => {
	appearOnScroll.observe(slider);
});