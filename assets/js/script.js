/*====== SHOW nav Menus ======*/
const toggleID = document.getElementById("nav_toggle");
const navId = document.getElementById("nav_menu");
let menuOpen = false;

try {
	if(toggleID === undefined) throw "undefined";
	toggleID.addEventListener("click", ()=>{
		if(!menuOpen) {
			navId.classList.add("show");
			menuOpen = true;
		}else{
			navId.classList.remove("show");
			menuOpen = false;
		}
	});
}
catch (err) {
	console.log(err);
}

/*==== Remove dropDownList class in footer(Mobile Mode) ====*/

const dropDownBtn = document.querySelectorAll(".dropDownBtn");
const dropDownList = document.querySelectorAll(".dropDownList");
dropDownBtn.forEach(n => n.addEventListener("click", removeDropDown));

function removeDropDown(event) {
	e = event.currentTarget;
	//dropDownList.forEach(n => n.classList.remove("dropDownList"));
	//dropDownList.classList.add("active");
	nextSibs = e.nextElementSibling
	if(nextSibs.classList.contains("dropDownList")) {
		nextSibs.classList.remove("dropDownList");
		nextSibs.classList.add("activeDropDown");
	}else{
		nextSibs.classList.remove("activeDropDown");
		nextSibs.classList.add("dropDownList");
	}

}

/*==== Remove active class in the links(Mobile Mode) ====*/

const navCAT = document.querySelectorAll(".navCat");
navCAT.forEach(n => n.addEventListener("click", linkAction));

function linkAction() {
	console.log(this);
	navCAT.forEach(n => n.classList.remove("active"));
	this.classList.add("active");

	//console.log(parentEl);
}

/*==== story image slider ====*/
const carousel = document.querySelector(".carousel_section");
const nextButton = document.querySelector(".right-btn");
const previousButton = document.querySelector(".left-btn");

try {
	const slides = [...carousel.children];
	let slideWidth = slides[0].getBoundingClientRect().width;

	if(slides === null) throw [];
	function positionSlides(slides) {
		for(let index = 0;index < slides.length; index++) {
			slides[index].style.left = slideWidth * index + "px";
		}
	}

	positionSlides(slides);

	nextButton.addEventListener("click", function() {
		const currentSlide = carousel.querySelector(".activeSlide");
		const nextSlide = currentSlide.nextElementSibling;
		moveToSlide(carousel, currentSlide, nextSlide);
		hideButton(nextSlide, slides);
	});

	previousButton.addEventListener("click", function() {
		const currentSlide = carousel.querySelector(".activeSlide");
		const previousSlide = currentSlide.previousElementSibling;
		moveToSlide(carousel, currentSlide, previousSlide);
		hideButton(previousSlide, slides);
	});

	function moveToSlide(carousel, currentSlide, targetSlide) {
		const position = targetSlide.style.left;
		carousel.style.transform = `translateX(-${position})`;
		toggleActive(currentSlide, targetSlide);
	}

	function toggleActive(current, target) {
		current.classList.remove("activeSlide");
		target.classList.add("activeSlide");
	}

	function hideButton(currentSlide, target) {
		if(target === slides[0]) {
			previousButton.classList.add("hide");
			nextButton.classList.remove("hide");
		}
		else if(target === slides[slides.length - 1]){
			nextButton.classList.add("hide");
			previousButton.classList.remove("hide");
		}
		else {
			nextButton.classList.remove("hide");
			previousButton.classList.remove("hide");
		}
	}

	function findIndex(item, items) {
		for(let index = 0; index < items.length; index++) {
			if(item === item[index]){
				return index;
			}
		}
	}

}
catch(err) {
	console.log(err);
}

/*==== Description Modal ====*/

const modalBtn = document.getElementById("modalBtn");
const specificationModal = document.getElementById("specificationModal");
const closeModal = document.querySelector(".close");


try {
	modalBtn.addEventListener("click", clickModal);
	if(modalBtn === undefined) throw "undefined";

	function clickModal() {
		specificationModal.style.visibility = "visible";
		specificationModal.classList.add("modal-active");
	}

	closeModal.addEventListener("click", closeModalSpecs);

	function closeModalSpecs() {
		specificationModal.style.visibility = "hidden";
		specificationModal.classList.remove("modal-active");
	}

	/*==== Delivery Modal ====*/

	const modalBtn2 = document.getElementById("modalBtn2");
	const specificationModal2 = document.getElementById("specificationModal2");
	const closeModal2 = document.querySelector(".close2");



	modalBtn2.addEventListener("click", clickModal2);

	function clickModal2() {
		specificationModal2.style.visibility = "visible";
		specificationModal2.classList.add("modal-active");
	}

	closeModal2.addEventListener("click", closeModalSpecs2);

	function closeModalSpecs2() {
		specificationModal2.style.visibility = "hidden";
		specificationModal2.classList.remove("modal-active");
	}

}
catch(err) {
	console.log(err);
}

//Add To Cart function
const addToCart = document.querySelector(".addToCartBtn");

try {
	if(addToCart === undefined) throw "undefined";
	
	addToCart.addEventListener("click", function(eventDetails) {
		const id = eventDetails.target.previousElementSibling.value;

		fetch(`http://localhost:8000/controller/addToCart.php?id=${id}`)
			.then(function (response) {
				// Converting the response body to text format
				// We can also use .json() if we are working with a JSON string
				return response.text()
			})
			.then(function (response) {
				// response in the next .then() corresponds 
				//to the converted response body from the previous .then()

				console.log(response);
				document.querySelector(".cart-count").innerText = response;
			})
	});
}
catch(err) {
	console.log(err);
}

//Update Quantity(plus)
document.querySelectorAll(".btn-plus").forEach(function (button) {
	button.addEventListener("click", function () {
		const button = this;
		const id = button.dataset.id;

		const price = parseFloat(button.parentElement.parentElement.previousElementSibling.firstElementChild.lastElementChild.firstElementChild.nextElementSibling.innerText);

		//Browser to call the quantity.php in server without reloading the page
		fetch(`http://localhost:8000/controller/updateQuantity.php?id=${id}&operation=plus`)
			.then(function (response) { return response.text() })
			.then(function (updatedQuantity) {
				// Get the updated value from updateQuantity.php
				button.nextElementSibling.innerText = updatedQuantity;

				// compute subtotal and apply to subtotal table data
				const subtotal = price * parseInt(updatedQuantity);
				//Note: Its important to simplify the DOM manipulation of every element so that we can get the precise
				//result that we wanted.
				//Example: the code below for subtotal is the simplify DOM manipulation of
				//button.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.firstElementChild.lastElementChild.lastElementChild.innerText;
				//the code above will only manipulate the 1st item of the cart, not the whole cart items.
				//So its best to simplify the DOM as much as possible
				button.parentElement.parentElement.previousElementSibling.firstElementChild.lastElementChild.lastElementChild.innerText = subtotal;

				let total = 0;
				let shippingFee = 0;
				let grandTotal = 0;

				const subPrice = document.querySelectorAll(".subPrice");
				document.querySelectorAll(".subPrice").forEach( function(subtotal) {
					total += parseInt(subtotal.innerText);
				});

				document.getElementById('subPriceTotal').innerText = total;
				if(subPrice.length >= 5 || updatedQuantity >= 5) {
					shippingFee = 0;
				}else{
					shippingFee = 200;
				}

				grandTotal = shippingFee + total;
				document.getElementById("grandTotal").innerText = "$" + grandTotal;
				document.getElementById("shippingFee").innerText = "$" + shippingFee;

				
			});
	})
});

//Update Quantity(minus)
document.querySelectorAll(".btn-minus").forEach(function (button) {
	button.addEventListener("click", function () {
		const button = this;
		const id = button.dataset.id;

		const price = parseFloat(button.parentElement.parentElement.previousElementSibling.firstElementChild.lastElementChild.firstElementChild.nextElementSibling.innerText);

		//Browser to call the quantity.php in server without reloading the page
		fetch(`http://localhost:8000/controller/updateQuantity.php?id=${id}&operation=minus`)
			.then(function (response) { return response.text() })
			.then(function (updatedQuantity) {
				// Get the updated value from updateQuantity.php
				button.previousElementSibling.innerText = updatedQuantity;
				if(parseInt(updatedQuantity) == 1) return;
				// compute subtotal and apply to subtotal table data
				const subtotal = price * parseInt(updatedQuantity);
				//Note: Its important to simplify the DOM manipulation of every element so that we can get the precise
				//result that we wanted.
				//Example: the code below for subtotal is the simplify DOM manipulation of
				//button.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.firstElementChild.lastElementChild.lastElementChild.innerText;
				//the code above will only manipulate the 1st item of the cart, not the whole cart items.
				//So its best to simplify the DOM as much as possible
				button.parentElement.parentElement.previousElementSibling.firstElementChild.lastElementChild.lastElementChild.innerText = subtotal;

				let total = 0;
				let shippingFee = 0;
				let grandTotal = 0;

				const subPrice = document.querySelectorAll(".subPrice");
				document.querySelectorAll(".subPrice").forEach( function(subtotal) {
					total += parseInt(subtotal.innerText);
				});

				document.getElementById('subPriceTotal').innerText = total;
				if(subPrice.length === 0) {
					shippingFee = 0;
				}

				if(subPrice.length >= 5 || updatedQuantity >= 5) {
					shippingFee = 0;
				}else{
					shippingFee = 200;
				}

				grandTotal = shippingFee + total;
				document.getElementById("grandTotal").innerText = "$" + grandTotal;
				document.getElementById("shippingFee").innerText = "$" + shippingFee;

				
			});
	})
});

//Image Slider
let counter = 1;
setInterval(function() {
	document.getElementById("radio" + counter).checked = true;
	counter++;
	if(counter > 4) {
		counter = 1;
	}
}, 5000);
