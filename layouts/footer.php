	<div class="push"></div>
  </div>
  <footer class="footer section text-center py-4 text-dark">
    <div class="footer_container bd_grid">
      <div class="footer_box">
        <h3 class="footer_title">Main Menu</h3>
        <i class="fas fa-caret-down dropDownBtn 1"></i>
        <ul class="dropDownList">
          <li><a href="<?php $path ?>/views/home.php" class="footer_link">HOME</a></li>
          <li class="footer_link"><a href="<?php $path ?>/views/clothing.php">CLOTHING</a></li>
          <li class="footer_link"><a href="<?php $path ?>/views/bread.php">BAKERY</a></li>
          <li class="footer_link"><a href="<?php $path ?>/views/jewelry.php">JEWELRY</a></li>
        </ul>
      </div>

      <div class="footer_box">
        <h3 class="footer_title">Services</h3>
        <i class="fas fa-caret-down dropDownBtn 2"></i>
          <ul class="dropDownList">
            <li class="#" class="footer_link"><a href="<?php $path ?>/views/message.php">Free Shipping</a></li>
            <li class="#" class="footer_link"><a href="<?php $path ?>/views/message.php">Special Discount</a></li>
          </ul>
      </div>

      <div class="footer_box">
        <h3 class="footer_title">Social Media Links</h3>
        <i class="fas fa-caret-down dropDownBtn 3"></i>
          <ul class="socialMediaLinks dropDownList">
            <li>
              <a href="https://www.facebook.com/cegisboutique/" class="footer_social"><i class="fab fa-facebook-square"></i>
            </li>
            <li>
              <a href="#" class="footer_social"><i class="fab fa-twitter-square"></i></a>
            </li>
            <li>
              <a href="#" class="footer_social"><i class="fab fa-google"></i></a>
            </li>
          </ul>
      </div>
    </div>

    <div>
      <div class="footer_container2 bd_grid">
        <div class="footer_boxes">
          <ul>
            <li><a href="<?php $path ?>/views/home.php" class="footer_link">HOME</a></li>
            <li class="footer_link"><a href="<?php $path ?>/views/clothing.php">CLOTHING</a></li>
            <li class="footer_link"><a href="<?php $path ?>/views/bread.php">BAKERY</a></li>
            <li class="footer_link"><a href="<?php $path ?>/views/jewelry.php">JEWELRY</a></li>
          </ul>
        </div>

        <div class="footer_boxes">
          <ul>
            <li><a href="<?php $path ?>/views/message.php" class="footer_link">Free Shipping</a></li>
            <li class="footer_link"><a href="<?php $path ?>/views/message.php">Special Discounts</a></li>
          </ul>
        </div>

        <div class="footer_boxes">
          <ul>
            <li>
              <a href="<?php $path ?>/views/message.php" class="footer_link">
                <i class="fab fa-facebook"></i> Facebook
              </a>
            </li>
            <li>
              <a href="<?php $path ?>/views/message.php" class="footer_link">
                <i class="fab fa-twitter-square"></i> Twitter
              </a>
            </li>
            <li>
              <a href="<?php $path ?>/views/message.php" class="footer_link">
                <i class="fab fa-google"></i> Google
              </a>
            </li>
          </ul>
        </div>

      </div>
    </div>

   <small>&copy; 2021 SAYUJI</small>
  </footer>

   <!-- CDN -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   
   <!-- Custom JS -->
   <script src="<?= $path ?>/assets/js/script.js"></script>
   <!-- Observer JS -->
   <script src="<?= $path ?>/assets/js/intersection-observer.js"></script>
</body>
</html>
