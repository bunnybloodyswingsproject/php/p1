<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta name="description" content="online shopping website for fashion clothes, customized cakes, and jewelry">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="shortcut icon" type="image/png" href="../assets/images/sayugi5_logo7.png">
	<title>Celigina</title>

	<!-- Metatags -->

	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">

   <!-- Fontawesome -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <script src="https://use.fontawesome.com/1a8df02521.js"></script>
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

   <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&family=Potta+One&display=swap" rel="stylesheet">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

   <!-- Custom CSS -->
   <link rel="stylesheet" type="text/css" href="<?= $path ?>/assets/css/style.css">

   <!-- LightBox CSS -->
   <link rel="stylesheet" type="text/css" href="<?php $path ?>/assets/css/lightbox.min.css">

   <!--Lightbox JS -->
   <script type="text/javascript" src="<?php $path ?>/assets/js/lightbox-plus-jquery.min.js"></script>


</head>
<body>

	<div class="wrapper">