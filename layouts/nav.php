<?php 
	$path = "..";
?>
<!-- Header -->

<header class="header-1">
	<nav class="nav">
		<div class="title_header">
			<a href="<?php $path ?>/views/home.php" class="nav_logo">SAYUJI</a>
			<div class="search_cart">
				<i class="fas fa-search"></i>
				<a href="<?php $path ?>/views/cart.php"><i class="fas fa-shopping-cart"></i></a>	
			</div>
		</div>

		<div class="nav_logo">
			<div class="nav_categories">
				<a href="<?php $path ?>/views/clothing.php" class="navCat"><i class="fas fa-tshirt"></i></a>
				<a href="<?php $path ?>/views/bread.php" class="navCat"><i class="fas fa-bread-slice"></i></i></a>
				<a href="<?php $path ?>/views/jewelry.php" class="navCat"><i class="far fa-gem"></i></a>
				<div class="nav_togIcon"><i class="fas fa-bars nav_toggle" id="nav_toggle"></i></div>
			</div>

			<div class="nav_menu" id="nav_menu">
				<ul class="nav_list">
					<li class="nav_item"><a href="<?= "http://".$_SERVER["HTTP_HOST"]."/views/home.php"?>" class="nav_link active">Home</a></li>
					<li class="nav_item"><a href="<?= "http://".$_SERVER["HTTP_HOST"]."/views/clothing.php"?>" class="nav_link active">Clothes</a></li>
					<li class="nav_item"><a href="<?= "http://".$_SERVER["HTTP_HOST"]."/views/bread.php"?>" class="nav_link active">Bread</a></li>
					<li class="nav_item"><a href="<?= "http://".$_SERVER["HTTP_HOST"]."/views/jewelry.php"?>" class="nav_link active">Jewelry</a></li>
					<li class="nav_item"><a href="<?= "http://".$_SERVER["HTTP_HOST"]."/views/message.php"?>" class="nav_link active">Message Us</a></li>
					<li class="nav_item cart-border"><a href="<?= "http://".$_SERVER["HTTP_HOST"]."/views/cart.php"?>" class="nav_link active">Cart <span class="badge badge-pill badge-light cart-count">
					<?php 
						if(!array_key_exists("cart", $_SESSION)) {
							$_SESSION["cart"] = [];
						}
						echo count($_SESSION["cart"]);
					?>
					</span></a></li>
					<?php 
						if(!array_key_exists("user", $_SESSION)) {
					?>
					
					<li class="nav_item"><a href="<?php $path ?>/views/login.php" class="nav_link active"><i class="fas fa-user"></i>Login</a></li>
					<li class="nav_item"><a href="<?php $path ?>/views/login.php" class="nav_link active"><i class="fas fa-user-plus"></i>Register</a></li>

					<?php
						}
					?>

					<?php 
						if(array_key_exists("user", $_SESSION)) {
					?>

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Hello! <?php echo $_SESSION["user"]["firstname"] ?>
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<?php $path ?>/views/transactionHistory.php">Orders</a>
							<a class="dropdown-item" href="<?php $path ?>/views/logout.php">Logout</a>
						</div>
					</li>

					<?php
						}
					?>
				</ul>
			</div>
		</div>
	</nav>
</header>


