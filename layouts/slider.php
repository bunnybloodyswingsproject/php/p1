
	<!-- ========= Box Slider ========= -->
	<section class="box_slider carouselNone">
		<div class="slide_container">
			<button class="btn left-btn hide">
				<i class="fas fa-angle-left"></i>
			</button>
			<div class="carousel-container">
				<ul class="carousel_section">
					<li class="slides activeSlide">
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/sayuji.png" class="story">
								<p>Our Name</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry1.jpg" class="has-story">
								<p>Marleyan Necklace</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry2.jpg" class="has-story">
								<p>The Royal Beads</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry3.jpg" class="has-story">
								<p>Clover Leaf</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry4.jpg" class="has-story">
								<p>Blueberry</p>
							</a>
						</div>
					</li>

					<li class="slides">
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry5.jpg" class="story">
								<p>Green Jade</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry6.jpg" class="has-story">
								<p>Diamond Ring</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry7.jpg" class="has-story">
								<p>Infinity Necklace</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry8.jpg" class="has-story">
								<p>Blue Spahirre</p>
							</a>
						</div>
						<div class="story-status">
							<a href="#">
								<img src="<?php $path ?>/assets/images/jewelry9.jpg" class="has-story">
								<p>The Ring</p>
							</a>
						</div>
					</li>

					<li class="slides">
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry10.jpg" class="story">
								<p>Plain Silver</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry11.jpg" class="has-story">
								<p>Rainbow Brace</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry12.jpg" class="has-story">
								<p>Royal Beads</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry14.jpg" class="has-story">
								<p>The Grandmaster</p>
							</a>
						</div>
						<div class="story-status">
							<a href="">
								<img src="<?php $path ?>/assets/images/jewelry15.png" class="has-story">
								<p>Pearly Sea</p>
							</a>
						</div>
					</li>
				</ul>
			</div>
			<button class="btn right-btn">
				<i class="fas fa-angle-right nextButton"></i>
			</button>
		</div>	
	</section>
